/*
 * protocol.cpp
 *
 * Created: 02/01/2022 14:21:20
 *  Author: Jens Elstner
 */
#include <string.h>
#include "protocol.hpp"

MCP_CAN can;
uint8_t can_status = CAN_FAIL;
// fragmented messages
CanId fragMsgId;
uint8_t fragMsgOffset;
uint16_t fragMsgSize;
uint8_t fragMsg[MAX_FRAG_MSG_SIZE * 4];
uint16_t elapsed_msecs = 0; // elapsed seconds in fixed point arithmetic with 10bit decimals (up to 64 seconds)

// callback to handle device specific messages
ProtocolMsgCallback msg_callback;

DeviceState device_state = STATE_CAN_TEST;
// firmware version: major.minor
uint16_t firmware_version;

// .userrow section is defined in linker settings
#define URMEM __attribute((section(".userrow")))
#define URADDR(var) (0xFF00 + (uint8_t *)&var - &USERROW.USERROW0)

enum device_flags_t : uint32_t
{
  // a firmware update has been scheduled
  FLAG_DEVICE_UPDATE_SCHEDULED = 1,
  // if set then we have a server-approved nat id, otherwise we need one
  FLAG_NAT_ID_SET = 2,
};

// USERROW data (32 bytes total)
struct device_settings_t
{
  uint16_t device_type;
  uint8_t hardware_version;
  device_flags_t flags;
  uint8_t can_bus_id;
  uint8_t reserved[4];
  // reserve max 16 bytes for configuration. if more is needed can be moved to eeprom
  uint8_t config[20];
};
URMEM device_settings_t device_settings;

#define HAS_NAT ((device_settings.flags & FLAG_NAT_ID_SET) == FLAG_NAT_ID_SET)

// used in communication with the server
#define DEVICE_SERIAL *((uint32_t *)&SIGROW.SERNUM6)

/************************************************************************/
/* Initialize the CAN communication                                     */
/************************************************************************/
uint8_t initialize_can()
{
  device_state = STATE_CAN_TEST;

  uint8_t res;
  int index;

  // enable CAN interrupt on PORT B2
  PORTB.DIRCLR = PIN_CAN_INT;
  PORTB.OUTSET = PIN_CAN_INT;
  PORTB.PIN2CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
  // acknowledge outstanding interrupt, just in case
  PORTB.INTFLAGS |= PIN_CAN_INT;

  // initialize CAN bus
  res = can.begin(MCP_ANY, CAN_500KBPS, MCP_8MHZ);
  if (res != CAN_OK)
  {
    return res;
  }

  uint8_t msg[] = {0xAA};
  res = can.sendMsgBuf(1, 1, 1, msg);
  if (res != CAN_OK)
  {
    return res;
  }

  // wait for the receive interrupt
  for (index = 0; index < 1000 && device_state == STATE_CAN_TEST; index++)
    ;

  res = can_status;

  // if test went ok, switch to normal mode
  if (device_state == STATE_CAN_LOOPBACK_OK && res == CAN_OK)
  {
    res = can.setMode(MCP_NORMAL);
  }
  return res;
}

/************************************************************************/
/* Get current CAN bus id from userrow                                  */
/************************************************************************/
uint8_t get_nat_id()
{
  if (!HAS_NAT)
  {
    // just use 0 as default NAT (TODO)
    return 0;
  }
  return device_settings.can_bus_id;
}

uint8_t update_can_filters()
{
  uint8_t res;

  // mask for direction and nat
  res = can.initMask(0, 1, 0x006FF000);
  if (res != MCP_CAN_OK)
    return res;
  res = can.initMask(1, 1, 0x006FF000);
  if (res != MCP_CAN_OK)
    return res;

  // filter for broadcast
  // RX0
  res = can.initFilter(0, 1, 0x006FF000);
  if (res != MCP_CAN_OK)
    return res;
  // RX1
  res = can.initFilter(2, 1, 0x006FF000);
  if (res != MCP_CAN_OK)
    return res;

  if (HAS_NAT)
  {
    uint32_t nat_filter = (((uint32_t)0x06) << 21) + (((uint32_t)get_nat_id()) << 12);

    // filter for nat
    // RX0
    res = can.initFilter(1, 1, nat_filter);
    if (res != MCP_CAN_OK)
      return res;
    // RX1
    res = can.initFilter(3, 1, nat_filter);
    if (res != MCP_CAN_OK)
      return res;
  }
  else
  {
    // reset filter for nat
    // RX0
    res = can.initFilter(1, 1, 0x00);
    if (res != MCP_CAN_OK)
      return res;
    // RX1
    res = can.initFilter(3, 1, 0x00);
    if (res != MCP_CAN_OK)
      return res;
  }

  return res;
}

uint8_t send_fragment_msg(uint8_t nat_id, uint8_t cmd, uint8_t *msg, uint16_t len)
{
  uint8_t res;

  // send fragment header first
  uint32_t id = CAN_ID(DIR_FROM_EXT, PKG_FRAG, nat_id, CMD_FRAG_HEADER);
  MsgFragHeader_t head;
  head.command = cmd;
  head.size = len;
  res = can.sendMsgBuf(id, 1, sizeof(head), (uint8_t *)&head);
  if (res != CAN_OK)
    return res;

  // send fragments
  id = CAN_ID(DIR_FROM_EXT, PKG_FRAG, nat_id, CMD_FRAG_DATA);
  uint8_t *p_msg = msg;
  for (uint16_t idx = 0; idx < len; idx += 8, p_msg += 8)
  {
    res = can.sendMsgBuf(id, 1, len - idx >= 8 ? 8 : len - idx, p_msg);
    if (res != CAN_OK)
      break;
  }
  return res;
}

/************************************************************************/
/* Send device information to the server                                */
/************************************************************************/
uint8_t send_start_info()
{
  uint8_t nat_id = get_nat_id();
  uint32_t device_serial = DEVICE_SERIAL;

  // check whether our NAT has the PARKED flag set, in that case we wait for activation by the server
  if (nat_id & 0x80)
  {
    device_state = STATE_PARKED;
  }
  else
  {
    device_state = STATE_RUNNING;
  }

  MsgStartInfo_t msg;
  msg.device_type = device_settings.device_type;
  msg.hardware_version = device_settings.hardware_version;
  msg.firmware_version = firmware_version;
  msg.device_serial = device_serial;

  return send_fragment_msg(nat_id, CMD_START_INFO, (uint8_t *)&msg, sizeof(MsgStartInfo_t));
}

/************************************************************************/
/* Request a NAT ID from the server                                     */
/************************************************************************/
uint8_t send_nat_offer_request()
{
  uint8_t nat_id = get_nat_id();
  uint32_t device_serial = DEVICE_SERIAL;

  device_state = STATE_OFFLINE;

  uint32_t id = CAN_ID(DIR_FROM_EXT, PKG_NORM, nat_id, CMD_NAT_OFFER_REQUEST);
  MsgNatOfferRequest_t msg;
  msg.device_type = device_settings.device_type;
  msg.device_serial = device_serial;
  return can.sendMsgBuf(id, 1, sizeof(MsgNatOfferRequest_t), (uint8_t *)&msg);
}

/************************************************************************/
/* Send a ping message to the server                                    */
/************************************************************************/
uint8_t send_ping()
{
  uint8_t nat_id = get_nat_id();
  uint32_t device_serial = DEVICE_SERIAL;

  uint32_t id = CAN_ID(DIR_FROM_EXT, PKG_NORM, nat_id, CMD_PING);
  MsgPing_t msg;
  msg.device_serial = device_serial;
  return can.sendMsgBuf(id, 1, sizeof(MsgPing_t), (uint8_t *)&msg);
}

/************************************************************************/
/* Send a message to the server                                         */
/************************************************************************/
uint8_t send_message(MSG_CMD cmd, uint8_t *msg, uint8_t len)
{
  if (device_state != STATE_RUNNING)
    return CAN_FAILTX;

  uint32_t id = CAN_ID(DIR_FROM_EXT, PKG_NORM, get_nat_id(), cmd);
  return can.sendMsgBuf(id, 1, len, msg);
}

/************************************************************************/
/* Verify the expected device type to the one stored in userrow         */
/************************************************************************/
bool check_device_type(uint16_t expected_device_type)
{
  return device_settings.device_type == expected_device_type;
}

/************************************************************************/
/* Start the CAN bus protocol                                           */
/************************************************************************/
bool protocol_start(uint16_t fw_version, ProtocolMsgCallback callback)
{
  firmware_version = fw_version;
  msg_callback = callback;

  device_state = STATE_CAN_TEST;
  if (initialize_can() != CAN_OK)
  {
    device_state = STATE_CAN_FAILED;
    return false;
  }

  if (update_can_filters() != MCP_CAN_OK)
  {
    // TODO
  }

  uint8_t res;
  if (HAS_NAT)
  {
    res = send_start_info();
  }
  else
  {
    res = send_nat_offer_request();
  }
  return res == CAN_OK;
}

/************************************************************************/
/* Process any received messages and implement the protocol             */
/************************************************************************/
void processCanMessages()
{
  uint8_t res;
  CanId id;
  uint8_t len_raw;
  uint8_t msg_raw[MAX_CHAR_IN_MESSAGE];

  uint8_t *msg = msg_raw;

  do
  {
    res = can.readMsgBuf(&id.raw, &len_raw, msg_raw);
    if (res != CAN_OK)
      continue;

    // point to the raw message again in case a fragmented message was completed before
    msg = msg_raw;

    // if this is the startup test, then store the status and ignore the message
    if (device_state == STATE_CAN_TEST)
    {
      can_status = res;
      device_state = STATE_CAN_LOOPBACK_OK;
      continue;
    }

    uint16_t len = len_raw;
    uint8_t nat_id = get_nat_id();
    uint32_t device_serial = DEVICE_SERIAL;

    // bit 7 in natId marks the device as parked
    if (id.bits.nat != NAT_BROADCAST && (id.bits.nat & 0x7F) != (nat_id & 0x7F))
    {
      // TODO: implement this filter in the CAN chip
      continue;
    }

    if (id.bits.pkg == PKG_FRAG)
    {
      bool frag_msg_complete = false;
      switch (id.bits.cmd)
      {
      case CMD_FRAG_HEADER:
      {
        MsgFragHeader_t *frag_head = (MsgFragHeader_t *)msg;
        fragMsgId.raw = id.raw;
        fragMsgId.bits.cmd = frag_head->command;
        fragMsgId.bits.pkg = PKG_NORM;
        fragMsgSize = frag_head->size;
        fragMsgOffset = 0;
        break;
      }
      case CMD_FRAG_DATA:
        // make sure the message is not too large
        if (fragMsgOffset + len >= MAX_FRAG_MSG_SIZE)
        {
          len = MAX_FRAG_MSG_SIZE - fragMsgOffset;
        }
        if (len > 0)
        {
          memcpy(&fragMsg[fragMsgOffset], msg, len);
          fragMsgOffset += len;
          // message finished
          if (fragMsgOffset == fragMsgSize || fragMsgOffset == MAX_FRAG_MSG_SIZE)
          {
            id.raw = fragMsgId.raw;
            msg = fragMsg;
            len = fragMsgSize;
            frag_msg_complete = true;
          }
        }
        break;
      }
      // if we processed the message but don't have a full fragmented message result yet then get the next message
      if (!frag_msg_complete)
        continue;
    }

    switch (id.bits.cmd)
    {
    case CMD_SET_OFFLINE:
      device_state = STATE_OFFLINE;
      break;
    case CMD_IDENTIFY_UNKNOWN:
      if (device_state == STATE_OFFLINE && !HAS_NAT)
      {
        res = send_nat_offer_request();
      }
      break;
    case CMD_NAT_OFFER_CONFIRM:
    {
      // got a new nat, check matching serial or it could be for someone else
      MsgNatOfferConfirm_t *msg_confirm = (MsgNatOfferConfirm_t *)msg;
      if (msg_confirm->device_serial == device_serial)
      {
        // update our NAT ID
        nat_id = msg_confirm->nat_id;
        eeprom_busy_wait();
        eeprom_write_byte((uint8_t *)URADDR(device_settings.can_bus_id), nat_id);
        eeprom_busy_wait();
        eeprom_write_dword((uint32_t *)URADDR(device_settings.flags), (uint32_t)(device_settings.flags | FLAG_NAT_ID_SET));
        eeprom_busy_wait();

        update_can_filters();

        res = send_start_info();
      }
      break;
    }
    case CMD_SEND_CONFIG:
      // if we have a nat_id and bit 7 is not set then switch to running state
      if ((device_settings.flags & FLAG_NAT_ID_SET) && !(nat_id & 0x80))
      {
        device_state = STATE_RUNNING;
      }
      if (msg_callback != 0)
      {
        msg_callback(id.bits.cmd, msg, len);
      }
      break;
    default:
      // call the callback for any command in the receive value range
      if (id.bits.cmd >= 0x80 && id.bits.cmd <= 0x8F)
      {
        msg_callback(id.bits.cmd, msg, len);
      }
    }
  } while (can.checkReceive() == CAN_MSGAVAIL);
}

/************************************************************************/
/* CAN interrupt handler                                                */
/************************************************************************/
void handleCanInterrupt()
{
  can.scheduleInterruptHandler(&processCanMessages);
}

/************************************************************************/
/* Timer interrupt handler - fixed point, 10bit decimals                */
/************************************************************************/
void handleTimer(uint16_t t_delta)
{
  elapsed_msecs += t_delta;

  uint8_t elapsed_secs = elapsed_msecs >> 10;

  if (elapsed_secs >= 60)
  {
    elapsed_msecs = 0;

    if (device_state == STATE_RUNNING || device_state == STATE_PARKED)
    {
      send_ping();
    }
    else if (device_state == STATE_OFFLINE)
    {
      // in case we were set offline and missed the nat confirm message try starting again
      send_start_info();
    }
  }
}
