/*
 * protocol.h
 *
 * Created: 24/11/2021 14:54:20
 * Author: Jens Elstner
 * Variables and helpers for the HeimCtrl CAN bus protocol
 */ 


#ifndef PROTOCOL_HPP_
#define PROTOCOL_HPP_

#include <avr/io.h>
#include <avr/eeprom.h>
#include "mcp_can.hpp"

// PORTB
#define PIN_CAN_INT   PIN2_bm

enum DeviceState {
  STATE_CAN_TEST = 0x00,
  STATE_CAN_LOOPBACK_OK = 0x01,
  STATE_RUNNING = 0x02,
  STATE_PARKED = 0x03,
  STATE_OFFLINE = 0x04,
  // failure states
  STATE_CAN_FAILED = 0x20,
};
extern DeviceState device_state;

/*
  Identifier format taken from Loxone
  00010000.0ddlnnnn.nnnn0000.cccccccc
  cccccccc contains the command byte.
  nnnnnnnn is the NAT (0x01�0x7E), which is the id of the extension on the Loxone Link bus. A NAT of 0xFF is a broadcast to all extensions. Bit 7 is set for parked extensions, which have been detected by the Miniserver, but are not configured in Loxone Config.
  dd is a direction/type of the command: 00 = sent from a extension/device, 10 = from a extension/device, when sending a shortcut messages back to the Miniserver, 11 = sent from the Miniserver
  l package type: 0 = regular package, 1 = fragmented package
*/
#define CAN_ID(dir, pkg, nat, cmd) (uint32_t)(0x10000000 + (((uint32_t)dir) << 21) + (((uint32_t)pkg) << 20) + (((uint32_t)nat) << 12) + ((uint32_t)cmd & 0xFF))

struct CanIdBits
{
  uint8_t cmd: 8;
  uint8_t :4;
  uint8_t nat: 8;
  uint8_t pkg: 1;
  uint8_t dir: 2;
};

union CanId
{
  uint32_t raw;
  CanIdBits bits;
};

#define MAX_FRAG_MSG_SIZE 32

enum MSG_DIR
{
  DIR_FROM_EXT        = 0b00,
  DIR_SHORTCUT_TO_SRV = 0b10,
  DIR_FROM_SRV        = 0b11,
};

enum MSG_PKG
{
  PKG_NORM        = 0,
  PKG_FRAG        = 1,
};

enum MSG_NAT
{
  NAT_BROADCAST = 0xFF,
  NAT_PARKED = 0x80,
};

enum DEVICE_TYPE
{
  DEV_POWERCTRLSOCKET = 0x10,
  DEV_POWERCTRLBLINDS = 0x11,
  DEV_SWITCHCTRL = 0x20,
  DEV_LIGHTCTRL = 0x30,
  DEV_LIGHTSTRIPCTRL = 0x31,
  DEV_DOORCTRL = 0x40,
};

// 0x00...0x7F: typically common commands shared by all devices
// 0x80...0xEF: send to/from devices to update values.
//   0x80...0x8F: regular value updates
// 0xF0...0xFF: device detection, NAT translation and handling of fragmented packages
enum MSG_CMD
{
  // common commands shared by all devices
  CMD_VERSION_REQ       = 0x01,
  CMD_START_INFO        = 0x02,
  CMD_VERSION_INFO      = 0x03,
  CMD_PING              = 0x05,
  CMD_PONG              = 0x06,
  CMD_SET_OFFLINE       = 0x07,
  CMD_TIMESYNC          = 0x0C,
  CMD_SEND_CONFIG       = 0x11,
  // device detection, NAT translation and handling of fragmented packages
  CMD_FRAG_HEADER       = 0xf0,
  CMD_FRAG_DATA         = 0xf1,
  CMD_FIRMWARE_UPDATE   = 0xf3,
  CMD_IDENTIFY_UNKNOWN  = 0xf4,
  CMD_SEARCH_DEV_REQ    = 0xfb,
  CMD_SEARCH_DEV_RES    = 0xfc,
  CMD_NAT_OFFER_CONFIRM = 0xfd,
  CMD_NAT_OFFER_REQUEST = 0xfe,
  // send to/from devices to update values
  CMD_DIGITAL_VALUE     = 0x80,
  CMD_ANALOG_VALUE      = 0x81,
  CMD_RGBW_VALUE        = 0x82,
  CMD_FREQUENCY_VALUE   = 0x85,
};

struct MsgFragHeader_t
{
  uint8_t command;
  uint16_t size;
};

struct MsgNatOfferRequest_t
{
  uint16_t reserved;
  uint16_t device_type;
  uint32_t device_serial;
};

struct MsgNatOfferConfirm_t
{
  uint8_t nat_id; 
  uint8_t reserved;
  uint16_t reserved2;
  uint32_t device_serial;
};

struct MsgStartInfo_t
{
  uint16_t device_type;
  uint8_t hardware_version;
  uint16_t firmware_version;
  uint32_t device_serial;
};

struct MsgPing_t
{
  uint32_t device_serial;
};

struct MsgTimeSync_t
{
  uint16_t year;
  uint8_t month;
  uint8_t day;
  uint32_t msSinceMidnight;
};

struct MsgConfigHead_t
{
  uint8_t size;
  uint8_t version;
  uint8_t reserved[6];
  uint8_t data[];
};

typedef void (*ProtocolMsgCallback)(uint8_t cmd, uint8_t* msg, uint16_t size);

// Verify the expected device type to the one stored in userrow
bool check_device_type(uint16_t expected_device_type);
// Start the CAN bus protocol
bool protocol_start(uint16_t fw_version, ProtocolMsgCallback callback = 0);
// CAN interrupt handler
void handleCanInterrupt();
// Timer handler - for regular ping messages and other timed events
void handleTimer(uint16_t t_delta);
// Send a message to the server
uint8_t send_message(MSG_CMD cmd, uint8_t* msg, uint8_t len);

#endif /* PROTOCOL_HPP_ */