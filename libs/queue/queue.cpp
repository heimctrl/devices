/*
 * queue.cpp
 *
 * Created: 03/04/2020 14:19:37
 *  Author: Jens Elstner
 */ 
#include "queue.h"

QUEUE_OP_T *queue;
uint8_t queue_length = 0;
uint8_t queue_start = 0;
uint8_t queue_end = 0;
uint16_t queue_resolution;
bool queue_paused = false;

QueueCallback exec_op = 0;


/***********************************************************
** Initialize the queue
** resolution: the smallest time unit for each queue step in ms.
**   a value of 10 would mean a minimum of 10ms between each queue operation
**   also the smallest possible pause between operations
** storage: storage array for queue operations
** max_length: the max number of ops in the storage array
***********************************************************/
void queue_init(uint16_t resolution, QUEUE_OP_T *storage, uint8_t max_length)
{
  queue_resolution = resolution;
  queue = storage;
  queue_length = max_length;
}

// define the callback for processing custom operations
void queue_set_callback(QueueCallback callback)
{
  exec_op = callback;
}

// push to end of queue
void queue_push(QUEUE_OP_T code)
{
  queue[queue_end] = code;
  queue_end++;
  if (queue_end >= queue_length)
  {
    queue_end = 0;
  }
}

// insert at beginning of queue
void queue_insert(QUEUE_OP_T code)
{
  if (queue_start == 0)
  {
    queue_start = queue_length;
  }
  queue_start--;
  queue[queue_start] = code;
}

// pop top value from queue
QUEUE_OP_T queue_pop()
{
  if (queue_start == queue_end) return 0;

  int8_t value = queue[queue_start];

  queue_start++;
  if (queue_start >= queue_length)
  {
    queue_start = 0;
  }

  return value;
}

// when pushing multi-value operations, pause processing so the interrupt doesn't corrupt the queue
void queue_pause()
{
  queue_paused = true;
}

// resume paused queue processing
void queue_resume()
{
  queue_paused = false;  
}

// queue a OP_PAUSE operation of the specified delay length
void queue_push_pause(QUEUE_OP_T delay)
{
  queue_push(OP_PAUSE);
  int16_t ticks = delay / queue_resolution;
  queue_push(ticks < 1 ? 1 : ticks);
}

bool exec_pause()
{
  QUEUE_OP_T duration = queue_pop();
  duration--;
  if (duration <= 0) return false;

  // decrement and keep going
  queue_insert(duration);
  queue_insert(OP_PAUSE);
  return true;
}

void queue_process_next()
{
  if (queue_paused)
  {
    return;
  }

  bool is_deferred = false;
  QUEUE_OP_T op;
  // loop until out of operations or deferred to next tick
  while(!is_deferred && (op = queue_pop()) != 0)
  {
    is_deferred = false;
    switch (op)
    {
      case OP_PAUSE:
      is_deferred = exec_pause();
      break;
      default:
      if (exec_op != 0)
      {
        is_deferred = exec_op(op);
      }
      break;
    }
  }
}
