/*
 * queue.h
 *
 * Created: 03/04/2020 14:20:27
 *  Author: Jens Elstner
 */ 


#ifndef QUEUE_H_
#define QUEUE_H_

#include <avr/io.h>

#define OP_PAUSE 0x01

typedef uint8_t QUEUE_OP_T;

typedef bool (*QueueCallback)(QUEUE_OP_T op);


void queue_init(uint16_t resolution, QUEUE_OP_T *storage, uint8_t max_length);    // initialize the rtc clock and interrupt for queue processing
void queue_set_callback(QueueCallback callback);  // define the callback for processing custom operations
void queue_process_next();                        // callback for the timer to process the next operation

void queue_push(QUEUE_OP_T code);                 // push to end of queue
void queue_insert(QUEUE_OP_T code);               // insert at beginning of queue
QUEUE_OP_T queue_pop();                           // pop top value from queue
void queue_push_pause(QUEUE_OP_T delay);          // queue a pause operation

void queue_pause();                               // when pushing multi-value operations, pause processing so the interrupt doesn't corrupt the queue
void queue_resume();                              // resume paused queue processing

#endif /* QUEUE_H_ */