/*
 * rtc.h
 *
 * Created: 24/01/2020 07:39:17
 *  Author: Creator
 */ 


#ifndef RTC_H_
#define RTC_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#define RTC_TO_FIXED(num) ((num) << 16)

typedef void (*RTCCallback)();

/***********************************************************
** Initialize the RTC with the target resolution and maximum timing error
** resolution: step size in ms
** error: maximum error in 1/1000th resolution
***********************************************************/
void rtc_init(int16_t resolution, int16_t error);
void rtc_init_1ms();
void rtc_set_callback(RTCCallback callback);  // set the callback that is invoked on every tick

#endif /* RTC_H_ */