/*
 * rtc.cpp
 *
 * Created: 21/01/2020 08:49:36
 * Author: Jens Elstner
 */ 
#include "rtc.hpp"

RTCCallback rtc_callback = 0;

typedef struct RTC_PARAMS
{
  RTC_CLKSEL_t clk_sel;
  RTC_PRESCALER_t prescaler;
  uint16_t period;
  int32_t error;
} RTC_PARAMS_t;

#define FREQ1K    1024
#define FREQ32K   32768

// find the best prescaler value with the target error
// the error is specified as fixed-point shifted by 16 bit
void rtc_calculate_prescaler(int32_t freq, int32_t resolution, int32_t maxError, RTC_PARAMS_t *params)
{
  int32_t prescaler;
  uint8_t prescaler_index;

  params->error = -1;

  // loop over all prescaler values
  for (prescaler = 1, prescaler_index = 0; prescaler <= freq; prescaler <<= 1, prescaler_index++)
  {
    int32_t perSec = freq >> prescaler_index;
    int32_t period = (perSec * resolution) / 1000;
    int32_t rest = (perSec * resolution) % 1000;

    // stop here since it will get worse with higher prescaler values
    if (period == 0) break;

    int32_t error = RTC_TO_FIXED(rest) / (perSec * resolution);
    if (error > maxError) break;

    params->prescaler = (RTC_PRESCALER_t) (prescaler_index << 3);
    params->period = period;
    params->error = error;
  }
}

void rtc_calculate_parameters(int32_t resolution, int32_t maxError, RTC_PARAMS_t *params)
{
  params->clk_sel = RTC_CLKSEL_INT1K_gc;
  rtc_calculate_prescaler(FREQ1K, resolution, maxError, params);
  if (params->error > maxError || params->error < 0)
  {
    params->clk_sel = RTC_CLKSEL_INT32K_gc;
    rtc_calculate_prescaler(FREQ32K, resolution, maxError, params);
  }
  // in this case we have not found any setting that stays below the minimum error
  // so we run as fast as we can
  if (params->error < 0)
  {
    params->clk_sel = RTC_CLKSEL_INT32K_gc;
    params->prescaler = RTC_PRESCALER_DIV1_gc;
    int32_t period = (FREQ32K * resolution) / 1000;
    int32_t rest = (FREQ32K * resolution) % 1000;
    params->period = period;
    params->error = RTC_TO_FIXED(rest) / (FREQ32K * resolution);
  }
}

/***********************************************************
** Initialize the RTC with the target resolution and maximum timing error
** resolution: step size in ms
** error: maximum error in 1/1000th resolution
***********************************************************/
void rtc_init(int16_t resolution, int16_t error = 1)
{
  // we will try and calculate a good delay value based on the requested resolution
  RTC_PARAMS_t params;
  rtc_calculate_parameters(resolution, error, &params);

  // 32.768kHz Internal Crystal Oscillator (INT32K)
  RTC.CLKSEL = params.clk_sel;

  // Wait for all registers to be synchronized
  while (RTC.STATUS > 0);

  // Set period value
  // the RTC counts up to RTC.PER and then overflows back to 0
  // an interrupt will be generated when the counter overflows RTC.PER
  RTC.PER = params.period;

  // Run in debug: enabled
  // RTC.DBGCTRL |= RTC_DBGRUN_bm;

  // Enable overflow Interrupt which will trigger ISR
  RTC.INTCTRL |= RTC_OVF_bm;

  RTC.CTRLA = params.prescaler          // 32768 / 32 = 1024/sec ~ 1 ms
  | RTC_RTCEN_bm                        // Enable: enabled
  | RTC_RUNSTDBY_bm;                    // Run In Standby: enabled
}

/***********************************************************
** Initialize the RTC with 0.9765625ms resolution (1/1024) for simplicity
***********************************************************/
void rtc_init_1ms()
{
  // 1kHz Internal Crystal Oscillator (INT1K)
  RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;

  // Wait for all registers to be synchronized
  while (RTC.STATUS > 0);

  // Set period value
  // the RTC counts up to RTC.PER and then overflows back to 0
  // an interrupt will be generated when the counter overflows RTC.PER
  RTC.PER = 31;

  // Run in debug: enabled
  RTC.DBGCTRL |= RTC_DBGRUN_bm;

  // Enable overflow Interrupt which will trigger ISR
  RTC.INTCTRL |= RTC_OVF_bm;

  RTC.CTRLA = RTC_PRESCALER_DIV1_gc // 1024 / 1 = 1024/sec = 0.9765625 ms
  | RTC_RTCEN_bm                    // Enable: enabled
  | RTC_RUNSTDBY_bm;                // Run In Standby: enabled
}

void rtc_set_callback(RTCCallback callback)
{
  rtc_callback = callback;
}

ISR(RTC_CNT_vect)
{
  // Clear flag by writing '1'
  if (RTC.INTFLAGS & RTC_OVF_bm)
  {
    RTC.INTFLAGS |= RTC_OVF_bm;
  
    if (rtc_callback != 0)
    {
      rtc_callback();
    }
  }
}