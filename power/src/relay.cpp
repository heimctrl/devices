/*
 * relay.cpp
 *
 * Created: 22/03/2020 07:56:53
 *  Author: Creator
 */ 
#include "relay.h"

void set_address(uint8_t pins);

/***********************************************************
** The timer counts at 1024/sec
** All timings for the relay management are in multiples of this delay
***********************************************************/
void relay_init()
{
  PORTA.DIRSET = PIN_SCLK + PIN_DIN + PIN_CS + PIN_RES;
  PORTA.OUTCLR = PIN_SCLK + PIN_DIN;
  PORTA.OUTSET = PIN_RES | PIN_CS;
}

bool exec_reset()
{
  PORTA.OUTCLR = PIN_RES;
  _delay_us(1);
  PORTA.OUTSET = PIN_RES;
  return false;
}

void set_address(uint8_t pins)
{
  uint8_t bit = 0x80;
  PORTA.OUTCLR = PIN_CS;
  
  do {
    if ((pins & bit) != 0) {
      PORTA.OUTSET = PIN_DIN;
    } else {
      PORTA.OUTCLR = PIN_DIN;
    }      
    _delay_us(1);
    PORTA.OUTSET = PIN_SCLK;
    _delay_us(1);
    PORTA.OUTCLR = PIN_SCLK;
    
    bit >>= 1;
  } while (bit > 0);

  PORTA.OUTSET = PIN_CS;
}

bool exec_set_address()
{
  QUEUE_OP_T pins = queue_pop();
  set_address(pins);
  return false;
}

bool relay_queue_callback(QUEUE_OP_T op)
{
  switch (op)
  {
    case OP_RESET:
    return exec_reset();
    case OP_SET_ADDRESS:
    return exec_set_address();
  }
  return false;
}

void switch_relay(int8_t relay, int8_t value)
{
  // there are 6 pins, starting at 1 (relay 1 ON), 2 (relay 1 OFF)
  int8_t pins = (value ? 2 : 1) << (relay * 2);

  queue_pause();

  // set the address and enable
  queue_push(OP_SET_ADDRESS);
  queue_push(pins);
  queue_push_pause(255);
  // reset
  queue_push(OP_RESET);

  queue_resume();
}
