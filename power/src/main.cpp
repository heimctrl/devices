/*
* tiny1614_test.cpp
*
* Created: 18/01/2020 12:36:51
* Author : Creator
*/
#define F_CPU 20000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include "rtc/rtc.h"
#include "queue/queue.h"
#include "mcp_can/mcp_can.h"
#include "relay.h"

#define RTC_RESOLUTION  10

// PORTB
#define PIN_CAN_INT   PIN2_bm
#define PIN_SPI_CS    PIN3_bm

enum DEVICE_STATE {
  STATE_CAN_TEST = 0x00,
  STATE_CAN_LOOPBACK_OK = 0x01,
  STATE_RUNNING = 0x02,
  // failure states
  STATE_CAN_FAILED = 0x20,
};
uint8_t device_state = STATE_CAN_TEST;

MCP_CAN can(PIN_SPI_CS);
uint8_t can_status = CAN_FAIL;

#define MAX_QUEUE_LENGTH  100
QUEUE_OP_T queue_storage[MAX_QUEUE_LENGTH];

#define PING_COUNTER_INIT (5000 / RTC_RESOLUTION)
uint16_t ping_counter = 0;

// count minutes since start
#define SECONDS_COUNTER_INIT (1000 / RTC_RESOLUTION)
uint16_t seconds_counter = SECONDS_COUNTER_INIT;
uint32_t seconds_since_start = 0;

// track relay state
uint8_t relay_state = 0;
uint8_t prev_relay_state = relay_state;

// temperature calibration coefficients
uint8_t sigrow_gain;
int8_t sigrow_offset;

// signal the current status with LED blink codes
void signal_status();

void receiveData()
{
  uint8_t res;
  uint32_t id;
  uint8_t len;
  uint8_t msg[MAX_CHAR_IN_MESSAGE];

  res = can.checkReceive();
  if (res == CAN_MSGAVAIL)
  {
    res = can.readMsgBuf(&id, &len, msg);
  }

  switch (device_state)
  {
    case STATE_CAN_TEST:
      // if this is the startup test, then store the status and ignore the message
      can_status = res;
      device_state = STATE_CAN_LOOPBACK_OK;
      break;
    case STATE_RUNNING:
      if (id == 0x124)
      {
        switch_relay(msg[0], msg[1]);
      }
      break;
  }
}

uint8_t initialize_can()
{
  device_state = STATE_CAN_TEST;

  uint8_t res;
  int index;

  // enable CAN interrupt on PORT B1
  PORTB.DIRCLR = PIN_CAN_INT;
  PORTB.PIN2CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;

  // initialize CAN bus
  res = can.begin(MCP_ANY, CAN_500KBPS, MCP_8MHZ);
  if (res != CAN_OK)
  {
    return res;
  }

  //res = can.initFilter(0, 0, 1);
  //if (res != CAN_OK)
  //{
  //return res;
  //}

  uint8_t msg[] = { 0xAA };
  res = can.sendMsgBuf(1, 1, msg);
  if (res != CAN_OK)
  {
    return res;
  }

  // wait for the receive interrupt
  for(index = 0; index < 1000 && device_state == STATE_CAN_TEST; index++);

  res = can_status;
  
  // if test went ok, switch to normal mode
  if (device_state == STATE_CAN_LOOPBACK_OK && res == CAN_OK)
  {
    res = can.setMode(MCP_NORMAL);
  }
  return res;
}

void init_temperature()
{
  VREF.CTRLA = VREF_ADC0REFSEL_1V1_gc;
  ADC0.CTRLA =
    ADC_RESSEL_10BIT_gc;      // 10 bit resolution    
  ADC0.CTRLB =
    // ADC_SAMPNUM_ACC1_gc;      // get 1 value
    ADC_SAMPNUM_ACC8_gc;      // get 8 values in one go for greater accuracy (averaged?)
  ADC0.CTRLC =
    ADC_SAMPCAP_bm            // reduced sample capacitance
    | ADC_REFSEL_INTREF_gc    // internal reference
    | ADC_PRESC_DIV16_gc;     // 20Mhz system clock -> 1.25Mhz ADC clock
  ADC0.CTRLD =
    ADC_INITDLY_DLY64_gc      // init delay > 40 (based on ADC clock, see datasheet)
    | ADC_ASDV_ASVON_gc       // sampling variation on
    | ADC_SAMPDLY0_bm;        // no sampling delay
  ADC0.SAMPCTRL = 64;         // sampling length > 40 (based on ADC clock, see datasheet)
  ADC0.MUXPOS = ADC_MUXPOS_TEMPSENSE_gc;

  sigrow_gain = SIGROW.TEMPSENSE0;
  sigrow_offset = SIGROW.TEMPSENSE1;

  ADC0.INTCTRL = ADC_RESRDY_bm;   // enable interrupt on result ready

  // enable
  ADC0.CTRLA |= ADC_ENABLE_bm;
}

void read_temperature()
{
  ADC0.COMMAND = ADC_STCONV_bm;
}

ISR(PORTB_PORT_vect)
{
  if ((PORTB.INTFLAGS & PIN_CAN_INT) != 0)
  {
    PORTB.INTFLAGS |= PIN_CAN_INT;
    can.scheduleInterruptHandler(&receiveData);
  }  
}

int16_t temperature, decimals;
ISR(ADC0_RESRDY_vect)
{
  if ((ADC0.INTFLAGS & ADC_RESRDY_bm) == 0)
  {
    return;
  }

  // ADC conversion result with 1.1 V internal reference
  // we are getting 8 samples accumulated so divide by 8
  uint16_t adc_reading = ADC0.RES >> 3;

  uint32_t temp = adc_reading - sigrow_offset;
  // Result might overflow 16 bit variable (10bit+8bit)
  temp *= sigrow_gain;
  // Add 1/2 to get correct rounding on division below
  // temp += 0x80;
  // Divide result to get Kelvin
  decimals = ((temp & 0xFF) * 10 + 0x80) >> 8;
  temp >>= 8;
  temperature = temp - 273;
}

uint16_t temp_delay = 0;
void rtc_callback()
{
  if (device_state != STATE_RUNNING)
  {
    return;
  }

  queue_process_next();

  if (temp_delay <= 0)
  {
    temp_delay = 3000 / RTC_RESOLUTION;
    read_temperature();
  }
  temp_delay--;

  // every X seconds send a ping
  if (ping_counter > 0) ping_counter--;
  if (ping_counter == 0)
  {
    uint8_t msg[] = { 0x10 };
    can.sendMsgBuf(0x1, 1, msg);
    ping_counter = PING_COUNTER_INIT;
  }

  // count minutes since start
  if (seconds_counter > 0) seconds_counter--;
  if (seconds_counter <= 0)
  {
    seconds_since_start++;
    seconds_counter = SECONDS_COUNTER_INIT;
  }

  // flip relay
  uint16_t second_of_minute = seconds_since_start % 60;
  uint16_t minute_of_hour = (seconds_since_start / 60) % 60;
  // turn on every 2 minutes for 20 seconds
  if ((minute_of_hour % 2) == 0) {
    if (second_of_minute >= 0 && second_of_minute < 20)
      relay_state = 1;
    else
      relay_state = 0;
  } else {
    relay_state = 0;
  }
  if (relay_state != prev_relay_state) {
    switch_relay(0, relay_state);
    prev_relay_state = relay_state;
  }

  // turn on for 3 minutes every 30 minutes
  //if ((minute_of_hour == 0 || minute_of_hour == 30) && relay_state == 0) {
    //relay_state = 1;
    //switch_relay(0, relay_state);
  //}
  //if ((minute_of_hour == 3 || minute_of_hour == 33) && relay_state == 1) {
    //relay_state = 0;
    //switch_relay(0, relay_state);
  //}
}

void blink_led(int8_t times)
{
  while (times > 0)
  {
    PORTB.OUTCLR = PIN_SPI_CS;
    _delay_ms(50);
    PORTB.OUTSET = PIN_SPI_CS;
    _delay_ms(100);
    times--;
  }
}

// signal the current status with LED blink codes
void signal_status()
{
  switch(device_state)
  {
    case STATE_CAN_FAILED:
    blink_led(2);
    break;
  }
}

int main(void)
{
  // just in case, disable external clock since we need PB2 and PB3 (TOSC1 and TOSC2)
  CLKCTRL.XOSC32KCTRLA &= ~CLKCTRL_ENABLE_bm;

  PORTB.DIRSET = PIN_SPI_CS;
  PORTB.OUTCLR = PIN_SPI_CS;
  // let the led shine for a bit to show we are online
  _delay_ms(1000);
  PORTB.OUTSET = PIN_SPI_CS;

  // Initialize relay outputs and rtc timer
  queue_init(RTC_RESOLUTION, queue_storage, MAX_QUEUE_LENGTH);
  relay_init();
  queue_set_callback(relay_queue_callback);
  // set rtc resolution to 10ms with max error of 5%
  rtc_init(RTC_RESOLUTION, RTC_TO_FIXED(5l) / 100);
  rtc_set_callback(rtc_callback);

  init_temperature();

  // enable interrupts
  sei();

  if (initialize_can() != CAN_OK)
  {
    device_state = STATE_CAN_FAILED;
    signal_status();
    return -1;
  }

  device_state = STATE_RUNNING;

  // set initial relay state
  switch_relay(0, relay_state);

  //switch_relay(0, 1);
  //_delay_ms(1000);
  //switch_relay(0, 0);
  //_delay_ms(1000);
  //switch_relay(1, 1);
  //_delay_ms(1000);
  //switch_relay(1, 0);
  //_delay_ms(1000);
  //switch_relay(2, 1);
  //_delay_ms(1000);
  //switch_relay(2, 0);

  // Set sleep mode to STANDBY mode
  set_sleep_mode(SLEEP_MODE_STANDBY);
  sleep_enable();
  sleep_cpu();

  /* Replace with your application code */
  while (1)
  {
  }
}

