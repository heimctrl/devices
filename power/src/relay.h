/*
 * relay.h
 *
 * Created: 22/03/2020 07:55:58
 *  Author: Creator
 */ 


#ifndef RELAY_H_
#define RELAY_H_

#define F_CPU 20000000UL
#include <avr/io.h>
#include <util/delay.h>
#include "queue/queue.h"
#include "rtc/rtc.h"

// PORTA
#define PIN_SCLK  PIN4_bm
#define PIN_DIN   PIN5_bm
#define PIN_CS    PIN6_bm
#define PIN_RES   PIN7_bm

enum OPERATION {
  OP_SET_ADDRESS = 0x42,
  OP_RESET = 0x43,
};

void relay_init();
bool relay_queue_callback(QUEUE_OP_T op);

void switch_relay(int8_t relay, int8_t value);

#endif /* RELAY_H_ */