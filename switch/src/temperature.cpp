/*
 * temperature.cpp
 *
 * Created: 02/01/2022 16:50:49
 *  Author: Creator
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include "temperature.h"

// temperature calibration coefficients
uint8_t sigrow_gain;
int8_t sigrow_offset;

int16_t temperature, decimals;


void init_temperature()
{
  VREF.CTRLA = VREF_ADC0REFSEL_1V1_gc;
  ADC0.CTRLA =
    ADC_RESSEL_10BIT_gc;      // 10 bit resolution    
  ADC0.CTRLB =
    // ADC_SAMPNUM_ACC1_gc;      // get 1 value
    ADC_SAMPNUM_ACC8_gc;      // get 8 values in one go for greater accuracy (averaged?)
  ADC0.CTRLC =
    ADC_SAMPCAP_bm            // reduced sample capacitance
    | ADC_REFSEL_INTREF_gc    // internal reference
    | ADC_PRESC_DIV16_gc;     // 20Mhz system clock -> 1.25Mhz ADC clock
  ADC0.CTRLD =
    ADC_INITDLY_DLY64_gc      // init delay > 40 (based on ADC clock, see datasheet)
    | ADC_ASDV_ASVON_gc       // sampling variation on
    | ADC_SAMPDLY_0_bm;        // no sampling delay
  ADC0.SAMPCTRL = 64;         // sampling length > 40 (based on ADC clock, see datasheet)
  ADC0.MUXPOS = ADC_MUXPOS_TEMPSENSE_gc;

  sigrow_gain = SIGROW.TEMPSENSE0;
  sigrow_offset = SIGROW.TEMPSENSE1;

  ADC0.INTCTRL = ADC_RESRDY_bm;   // enable interrupt on result ready

  // enable
  ADC0.CTRLA |= ADC_ENABLE_bm;
}

void read_temperature()
{
  ADC0.COMMAND = ADC_STCONV_bm;
}

ISR(ADC0_RESRDY_vect)
{
  if ((ADC0.INTFLAGS & ADC_RESRDY_bm) == 0)
  {
    return;
  }

  // ADC conversion result with 1.1 V internal reference
  // we are getting 8 samples accumulated so divide by 8
  uint16_t adc_reading = ADC0.RES >> 3;

  uint32_t temp = adc_reading - sigrow_offset;
  // Result might overflow 16 bit variable (10bit+8bit)
  temp *= sigrow_gain;
  // Add 1/2 to get correct rounding on division below
  // temp += 0x80;
  // Divide result to get Kelvin
  decimals = ((temp & 0xFF) * 10 + 0x80) >> 8;
  temp >>= 8;
  temperature = temp - 273;
}
