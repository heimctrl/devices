/*
* tiny1614_test.cpp
*
* Created: 18/01/2020 12:36:51
* Author : Creator
*/
#define F_CPU 3200000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include "main.hpp"
#include "temperature.h"
#include "mcp_can/mcp_can.h"
#include "mcp_can/protocol.hpp"
#include "rtc/rtc.h"

// firmware version: major.minor
#define FIRMWARE_VERSION 0x0001

#define SECS_TO_RTC(secs) (secs * 1024)
#define MS_TO_RTC(ms) (ms * 1024 / 1000)

// PORTB
#define PIN_CAN_INT   PIN2_bm

#define PIN_COUNT 6
PORT_t* pin_port[PIN_COUNT] = {
  &PORTA,
  &PORTA,
  &PORTA,
  &PORTA,
  &PORTB,
  &PORTB,
};

register8_t pin_mask[PIN_COUNT] = {
  PIN4_bm,
  PIN5_bm,
  PIN6_bm,
  PIN7_bm,
  PIN1_bm,
  PIN0_bm,
};

register8_t* pin_int[PIN_COUNT] = {
  &PORTA.PIN4CTRL,
  &PORTA.PIN5CTRL,
  &PORTA.PIN6CTRL,
  &PORTA.PIN7CTRL,
  &PORTB.PIN1CTRL,
  &PORTB.PIN0CTRL,
};

// define complete masks for both ports that include all input pins for faster operations
#define PORTA_MASK  (PIN4_bm | PIN5_bm | PIN6_bm | PIN7_bm)
#define PORTB_MASK  (PIN1_bm | PIN0_bm)

// fixed point pin state with 4 decimal places
#define DEBOUNCE_FIXPOINT 10000
#define DEBOUNCE_THRESHOLD_HIGH 7500
#define DEBOUNCE_THRESHOLD_LOW 2500
uint16_t debounced_pin_value[PIN_COUNT];
uint16_t current_pin_value[PIN_COUNT];
bool cur_pin_state[PIN_COUNT];
bool last_pin_state[PIN_COUNT];

uint8_t debounced_pins_count;
uint8_t debounced_pins_mask;
uint8_t debounced_pins[PIN_COUNT];
uint8_t interrupt_pins_count;
uint8_t interrupt_pins_mask;
uint8_t interrupt_pins[PIN_COUNT];

EnabledEvent_t enabled_events[MAX_EVENTS];
uint8_t enabled_event_count;


/************************************************************************/
/* Send a digital value                                                 */
/************************************************************************/
uint8_t send_digital_value(uint8_t event)
{
  MsgValueSwitch_t msg;
  msg.version = 1;
  msg.event = event;
  return send_message(CMD_DIGITAL_VALUE, (uint8_t*)&msg, sizeof(MsgValueSwitch_t));
}


void handleCanMessage(uint8_t cmd, uint8_t* msg, uint16_t size)
{
  switch(cmd)
  {
    case CMD_SEND_CONFIG:
    {
      // received configuration, check size and store in userrow
      MsgConfigHead_t* msg_config = (MsgConfigHead_t*)msg;
      uint8_t expected_size = 2 + ((MsgConfigSwitchV1_t*)msg_config->data)->num_events * sizeof(EventConfigV1_t);
      if (msg_config->size == expected_size && msg_config->version == CONFIG_VERSION)
      {
        eeprom_busy_wait();
        eeprom_update_block(msg_config->data, (void*)DEVICE_CONFIG_ADDR, msg_config->size);

        initialize_switch();
      }
    }
    break;
    case CMD_DIGITAL_VALUE:
    {
      // received a value, check size and store in userrow
      if (size == sizeof(MsgValueSwitch_t))
      {
        eeprom_busy_wait();
        eeprom_update_block(msg, (void*)DEVICE_VALUE_ADDR, sizeof(MsgValueSwitch_t));
      }
    }
    break;
  }
}

// Process PORTA and PORTB interrupts. The pins numbers given need to match the correct range for the PORT
void process_pin_interrupt(uint8_t start_pin, uint8_t end_pin, uint8_t active_mask)
{
  for (uint8_t pin = start_pin; pin < end_pin; pin++)
  {
    register8_t mask = pin_mask[pin];

    // this pin triggered
    if ((active_mask & mask) != 0)
    {
      PORT_t* PORT = pin_port[pin];
      uint8_t pin_status = PORT->IN & mask;

      // debounce_millis is in 10ms intervals
      current_pin_value[pin] = pin_status != 0 ? (DEBOUNCE_FIXPOINT / 10) / device_config->debounce_millis : 0;

      // TODO: more efficient structure for processing events per pin
      //for (uint8_t evt_idx = 0; evt_idx < MAX_EVENTS; evt_idx++)
      //{
        //EventConfigV1_t* event = &device_config->events[evt_idx];
        //if (!event->enabled || event->pin != pin) continue;
//
//
      //}
    }
  }
}

ISR(PORTA_PORT_vect)
{
  uint8_t int_flags = PORTA.INTFLAGS;
  PORTA.INTFLAGS |= 0xFF;
  if ((int_flags & PORTA_MASK) != 0)
  {
    process_pin_interrupt(0, 4, int_flags);
  }
}

ISR(PORTB_PORT_vect)
{
  uint8_t int_flags = PORTB.INTFLAGS;
  PORTB.INTFLAGS |= 0xFF;
  if ((int_flags & PIN_CAN_INT) != 0)
  {
    PORTB.INTFLAGS |= PIN_CAN_INT;
    handleCanInterrupt();
  }  

  if ((int_flags & PORTB_MASK) != 0)
  {
    process_pin_interrupt(4, PIN_COUNT, int_flags);
  }
}

void initialize_switch()
{
  enabled_event_count = 0;
  debounced_pins_count = 0;
  debounced_pins_mask = 0;
  interrupt_pins_count = 0;
  interrupt_pins_mask = 0;

  // enable switch pins (set as input)
  // pin A is the output, pin B the input
  // an interrupt is triggered when switching on and off
  PORTA.DIRCLR = PORTA_MASK;
  PORTB.DIRCLR = PORTB_MASK;
  // disable interrupts for all pins and only reenable the needed ones later
  for (uint8_t pin = 0; pin < PIN_COUNT; pin++)
  {
    *pin_int[pin] = PORT_ISC_INTDISABLE_gc;
  }

  for (uint8_t idx = 0; idx < device_config->num_events && idx < MAX_EVENTS; idx++)
  {
    EventConfigV1_t* event = &device_config->events[idx];
    if (!event->enabled) continue;

    uint8_t pin = event->pin;

    // configure interrupt
    register8_t int_flags = PORT_ISC_BOTHEDGES_gc;
    if (event->pullup) int_flags |= PORT_PULLUPEN_bm;
    *pin_int[pin] = int_flags;

    last_pin_state[pin] = cur_pin_state[pin] = (pin_port[pin]->IN & pin_mask[pin]) != 0;

    uint8_t pin_bit_mask = 1 << pin;
    if (event->debounce && device_config->debounce_millis > 0)
    {
      // set pin to be tracked in the timer interrupt and store the current value
      if ((debounced_pins_mask & pin_bit_mask) == 0) {
        debounced_pins[debounced_pins_count++] = pin;
        debounced_pins_mask |= pin_bit_mask;
      }
      debounced_pin_value[pin] = cur_pin_state[pin] ? DEBOUNCE_FIXPOINT : 0;
      // debounce_millis is in 10ms intervals
      current_pin_value[pin] = cur_pin_state[pin] ? (DEBOUNCE_FIXPOINT / 10) / device_config->debounce_millis : 0;
    } else if ((interrupt_pins_mask & pin_bit_mask) == 0) {
        interrupt_pins[interrupt_pins_count++] = pin;
        interrupt_pins_mask |= pin_bit_mask;
    }

    EnabledEvent_t* ev = &enabled_events[enabled_event_count++];
    ev->index = idx;
    ev->pin = event->pin;
    ev->type = event->type;
    ev->on_state = event->on_state;
    ev->debounce = event->debounce;
  }
}

uint16_t temp_delay = 0;
bool pin_state;
void rtc_callback()
{
  //wdt_reset();
  // allow the protocol handler to do timer things
  handleTimer(1);

  if (device_state != STATE_RUNNING)
  {
    signal_status();
    return;
  }

  // track exponential average value of debounced pins
  if (debounced_pins_count > 0)
  {
    uint16_t debounce_factor = (uint16_t)device_config->debounce_millis * 10;

    bool pin_changed = false;
    for (uint8_t idx = 0; idx < debounced_pins_count; idx++)
    {
      uint8_t pin = debounced_pins[idx];

      // exponential moving average: avg - (avg / factor) + (value / factor)
      uint16_t pin_value = debounced_pin_value[pin] - (debounced_pin_value[pin] / debounce_factor) + current_pin_value[pin];
      debounced_pin_value[pin] = pin_value;
      if (pin_value <= DEBOUNCE_THRESHOLD_LOW || pin_value >= DEBOUNCE_THRESHOLD_HIGH)
      {
        pin_state = pin_value >= DEBOUNCE_THRESHOLD_HIGH;
        if (pin_state != cur_pin_state[pin])
        {
          last_pin_state[pin] = cur_pin_state[pin];
          cur_pin_state[pin] = pin_state;
          pin_changed = true;
        }
      }
    }

    if (pin_changed)
    {
      for (uint8_t idx = 0; idx < enabled_event_count; idx++)
      {
        EnabledEvent_t event = enabled_events[idx];
        uint8_t pin = event.pin;
        // check whether the event uses a debounced pin
        if (!event.debounce || !(debounced_pins_mask & (1 << pin))) continue;

        bool pin_state = cur_pin_state[pin];
        event.last_state = pin_state;
        switch (event.type)
        {
          case EVENT_TYPE_SINGLE:
            if (event.on_state == pin_state)
            {
              send_digital_value(event.index);
            }
          break;
        }
      }
    }
  }

  if (temp_delay <= 0)
  {
    temp_delay = SECS_TO_RTC(3);
    read_temperature();
  }
  temp_delay--;
}

void blink_led(int8_t times)
{
  while (times > 0)
  {
    PORTB.OUTCLR = PIN_SPI_CS;
    _delay_ms(50);
    PORTB.OUTSET = PIN_SPI_CS;
    _delay_ms(100);
    times--;
  }      
}

// signal the current status with LED blink codes
void signal_status()
{
  switch(device_state)
  {
    case STATE_CAN_FAILED:
      blink_led(2);
      break;
    default:
      break;
  }
}

int main(void)
{
  PORTB.DIRSET = PIN_SPI_CS;
  PORTB.OUTSET = PIN_SPI_CS;

  if (!check_device_type(DEV_SWITCHCTRL))
  {
    // keep led on
    PORTB.OUTCLR = PIN_SPI_CS;
    return -1;
  }

  // set rtc resolution to 10ms with max error of 5%
  // rtc_init(RTC_RESOLUTION, RTC_TO_FIXED(5l) / 100);
  rtc_init_1ms();
  rtc_set_callback(rtc_callback);

  // enable interrupts
  sei();

  if (!protocol_start(FIRMWARE_VERSION, handleCanMessage))
  {
    signal_status();
    return -1;
  }

  initialize_switch();

  //RSTCTRL.RSTFR &= ~RSTCTRL_WDRF_bm;  // set watchdog reset flag to 0 (this will be 1 after a watchdog reset)
  //WDT.CTRLA = WDT_PERIOD_8CLK_gc;     // 8ms check period, we will reset this every 1ms in the timer interrupt

  // Set sleep mode to STANDBY mode
  set_sleep_mode(SLEEP_MODE_STANDBY);
  sleep_enable();
  sleep_cpu();

  /* Replace with your application code */
  while (1)
  {
  }
}

