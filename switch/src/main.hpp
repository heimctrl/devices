#ifndef _MAIN_HPP_
#define _MAIN_HPP_

// set clock prescaler to 1 o we get 20MHz
#define ENABLE_20MHZ CCP = CCP_IOREG_gc; CLKCTRL.MCLKCTRLB = 0;

// command 0x80, receive the current value
struct MsgValueSwitch_t
{
  uint8_t version;  // version of the message
  uint8_t event;    // the event index that was triggered
};

#define CONFIG_VERSION  1
#define MAX_EVENTS  6

enum EventType
{
  EVENT_TYPE_SINGLE = 0,
  EVENT_TYPE_DOUBLE = 1,
  EVENT_TYPE_TRIPLE = 2,
  EVENT_TYPE_LONG = 3,
  EVENT_TYPE_HOLD = 4,
};

struct EventConfigV1_t
{
  // byte 0
  bool enabled    : 1;
  uint8_t pin     : 3;
  EventType type  : 3;  // 0: single, 1: double, 2: triple, 3: long, 4: hold-release
  bool on_state   : 1;  // 0: low, 1: high
  // byte 1
  bool pullup     : 1;  // 0: off, 1: pullup enabled
  bool debounce   : 1;  // 0: off, 1: debounce enabled
};

struct MsgConfigSwitchV1_t
{
  uint8_t debounce_millis   : 4;  // how much time to use for smoothing out button presses for debouncing (value * 10ms)
  uint8_t long_press_millis : 4;  // how long to hold at least to register a long press (value * 100ms)
  uint8_t num_events;             // number of events in the events list
  EventConfigV1_t events[MAX_EVENTS];
};

static_assert(sizeof(MsgConfigSwitchV1_t) <= 24, "MsgConfigSwitchV1_t struct is too large!");

MsgConfigSwitchV1_t* device_config = (MsgConfigSwitchV1_t*)&USERROW.USERROW12;
#define DEVICE_CONFIG_ADDR  (0xFF00 + (uint8_t*)device_config - &USERROW.USERROW0)

MsgValueSwitch_t* current_value = (MsgValueSwitch_t*)&USERROW.USERROW26;
#define DEVICE_VALUE_ADDR  (0xFF00 + (uint8_t*)current_value - &USERROW.USERROW0)


struct EnabledEvent_t
{
  uint8_t index;
  uint8_t pin;
  EventType type;
  bool on_state;
  bool debounce;

  bool last_state;
  uint16_t last_state_ms;
};

// signal the current status with LED blink codes
void signal_status();
void initialize_switch();

#endif
/*********************************************************************************************************
 *  END FILE
 *********************************************************************************************************/
