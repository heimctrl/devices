LED strip driver
================

Drives a WS2815 LED strip of arbitrary length.


# WS2815 power consumption

For 150 LEDs

White level	Power
