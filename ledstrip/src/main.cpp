/*
* tiny1614_test.cpp
*
* Created: 18/01/2020 12:36:51
* Author : Creator
*/
#include "main.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include "tinyNeoPixel_Static.h"
#include "mcp_can/mcp_can.h"
#include "rtc/rtc.h"

#define RTC_RESOLUTION  10

// PORTA
#define PIN_SPI_SCK   PIN3_bm
#define PIN_SPI_MISO  PIN2_bm
#define PIN_SPI_MOSI  PIN1_bm
#define PIN_PWM_A     PIN4_bm
#define PIN_PWM_B     PIN5_bm
// PORTB
#define PIN_CAN_INT   PIN2_bm
#define PIN_SPI_CS    PIN3_bm
#define PIN_MOTOR_STBY   PIN1_bm

#define LED_PIN   0
#define NUM_LEDS  150
uint8_t pixels[NUM_LEDS * 3];
tinyNeoPixel LEDSTRIP(NUM_LEDS, LED_PIN, NEO_GRB, pixels);

enum DEVICE_STATE {
  STATE_CAN_TEST = 0x00,
  STATE_CAN_LOOPBACK_OK = 0x01,
  STATE_RUNNING = 0x02,
  // failure states
  STATE_CAN_FAILED = 0x20,
};
uint8_t device_state = STATE_CAN_TEST;

MCP_CAN can(PIN_SPI_CS);
uint8_t can_status = CAN_FAIL;

#define PING_COUNTER_INIT (5000 / RTC_RESOLUTION)
uint16_t ping_counter = 0;

#define DIM_MAX 500
uint16_t dim_cur = 0;
int16_t dim_direction = -1;
bool pwm_ready = false;

// temperature calibration coefficients
uint8_t sigrow_gain;
int8_t sigrow_offset;

// signal the current status with LED blink codes
void signal_status();

void receiveData()
{
  uint8_t res;
  uint32_t id;
  uint8_t len;
  uint8_t msg[MAX_CHAR_IN_MESSAGE];

  res = can.checkReceive();
  if (res == CAN_MSGAVAIL)
  {
    res = can.readMsgBuf(&id, &len, msg);
  }

  switch (device_state)
  {
    case STATE_CAN_TEST:
      // if this is the startup test, then store the status and ignore the message
      can_status = res;
      device_state = STATE_CAN_LOOPBACK_OK;
      break;
    case STATE_RUNNING:
      if (id == 0x124)
      {
        // TODO
      }
      break;
  }
}

ISR(PORTB_PORT_vect)
{
  if ((PORTB.INTFLAGS & PIN_CAN_INT) != 0)
  {
    PORTB.INTFLAGS |= PIN_CAN_INT;
    can.scheduleInterruptHandler(&receiveData);
  }  
}

uint8_t initialize_can()
{
  device_state = STATE_CAN_TEST;

  uint8_t res;
  int index;

  // enable CAN interrupt on PORT B1
  PORTB.DIRCLR = PIN_CAN_INT;
  PORTB.PIN2CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;

  // initialize CAN bus
  res = can.begin(MCP_ANY, CAN_500KBPS, MCP_8MHZ);
  if (res != CAN_OK)
  {
    return res;
  }

  //res = can.initFilter(0, 0, 1);
  //if (res != CAN_OK)
  //{
  //return res;
  //}

  uint8_t msg[] = { 0xAA };
  res = can.sendMsgBuf(1, 1, msg);
  if (res != CAN_OK)
  {
    return res;
  }

  // wait for the receive interrupt
  for(index = 0; index < 1000 && device_state == STATE_CAN_TEST; index++);

  res = can_status;
  
  // if test went ok, switch to normal mode
  if (device_state == STATE_CAN_LOOPBACK_OK && res == CAN_OK)
  {
    res = can.setMode(MCP_NORMAL);
  }
  return res;
}

void init_temperature()
{
  VREF.CTRLA = VREF_ADC0REFSEL_1V1_gc;
  ADC0.CTRLA =
    ADC_RESSEL_10BIT_gc;      // 10 bit resolution    
  ADC0.CTRLB =
    // ADC_SAMPNUM_ACC1_gc;      // get 1 value
    ADC_SAMPNUM_ACC8_gc;      // get 8 values in one go for greater accuracy (averaged?)
  ADC0.CTRLC =
    ADC_SAMPCAP_bm            // reduced sample capacitance
    | ADC_REFSEL_INTREF_gc    // internal reference
    | ADC_PRESC_DIV16_gc;     // 20Mhz system clock -> 1.25Mhz ADC clock
  ADC0.CTRLD =
    ADC_INITDLY_DLY64_gc      // init delay > 40 (based on ADC clock, see datasheet)
    | ADC_ASDV_ASVON_gc       // sampling variation on
    | ADC_SAMPDLY0_bm;        // no sampling delay
  ADC0.SAMPCTRL = 64;         // sampling length > 40 (based on ADC clock, see datasheet)
  ADC0.MUXPOS = ADC_MUXPOS_TEMPSENSE_gc;

  sigrow_gain = SIGROW.TEMPSENSE0;
  sigrow_offset = SIGROW.TEMPSENSE1;

  ADC0.INTCTRL = ADC_RESRDY_bm;   // enable interrupt on result ready

  // enable
  ADC0.CTRLA |= ADC_ENABLE_bm;
}

void read_temperature()
{
  ADC0.COMMAND = ADC_STCONV_bm;
}

int16_t temperature, decimals;
ISR(ADC0_RESRDY_vect)
{
  if ((ADC0.INTFLAGS & ADC_RESRDY_bm) == 0)
  {
    return;
  }

  // ADC conversion result with 1.1 V internal reference
  // we are getting 8 samples accumulated so divide by 8
  uint16_t adc_reading = ADC0.RES >> 3;

  uint32_t temp = adc_reading - sigrow_offset;
  // Result might overflow 16 bit variable (10bit+8bit)
  temp *= sigrow_gain;
  // Add 1/2 to get correct rounding on division below
  // temp += 0x80;
  // Divide result to get Kelvin
  decimals = ((temp & 0xFF) * 10 + 0x80) >> 8;
  temp >>= 8;
  temperature = temp - 273;
}

uint16_t pwm_clr;
void initialize_pwm(uint16_t initial = 100)
{
  // enable pwm on first output
  PORTA.DIRSET = PIN_PWM_A | PIN_PWM_B | PIN6_bm | PIN7_bm;

  PORTB.DIRSET = PIN_MOTOR_STBY;
  PORTB.OUTSET = PIN_MOTOR_STBY;

  CPU_CCP = CCP_IOREG_gc;
  TCD0.FAULTCTRL = TCD_CMPAEN_bm | TCD_CMPBEN_bm;
  TCD0.DBGCTRL = TCD_DBGRUN_bm;

  TCD0.CTRLB = TCD_WGMODE_DS_gc;
  pwm_clr = 20000000 / (1 * 2 * 100000) - 1;
  TCD0.CMPACLR = pwm_clr;
  TCD0.CMPBCLR = pwm_clr;
  TCD0.CMPASET = (100 - initial) * pwm_clr / 100;
  TCD0.CMPBSET = (100 - initial) * pwm_clr / 100;

  while(!(TCD0.STATUS & TCD_ENRDY_bm));
  TCD0.CTRLA = TCD_CLKSEL_20MHZ_gc | TCD_CNTPRES_DIV1_gc | TCD_ENABLE_bm;
  pwm_ready = true;
}

void set_pwm_a(uint16_t value = 100)
{
  while(!(TCD0.STATUS & TCD_CMDRDY_bm));
  TCD0.CMPASET = (DIM_MAX - value) * pwm_clr / DIM_MAX;
  TCD0.CTRLE = TCD_SYNCEOC_bm;
}

void set_pwm_b(uint16_t value = DIM_MAX)
{
  while(!(TCD0.STATUS & TCD_CMDRDY_bm));
  TCD0.CMPBSET = (DIM_MAX - value) * pwm_clr / DIM_MAX;
  TCD0.CTRLE = TCD_SYNCEOC_bm;
}

uint16_t temp_delay = 0;
void rtc_callback()
{
  if (device_state != STATE_RUNNING)
  {
    signal_status();
    return;
  }

  if (temp_delay <= 0)
  {
    temp_delay = 3000 / RTC_RESOLUTION;
    read_temperature();
  }
  temp_delay--;
  
  // every X seconds send a ping
  if (ping_counter > 0) ping_counter--;
  if (ping_counter == 0)
  {
    uint8_t msg[] = { 0x10 };
    can.sendMsgBuf(0x1, 1, msg);
    ping_counter = PING_COUNTER_INIT;
  }

  if (pwm_ready) {
    set_pwm_b(dim_cur);
    dim_cur += dim_direction;
    if (dim_cur <= 0) {
      dim_cur = 0;
      dim_direction = 1;
    }        
    if (dim_cur >= DIM_MAX) {
      dim_cur = DIM_MAX;
      dim_direction = -1;
    }
  }  
}

void blink_led(int8_t times)
{
  while (times > 0)
  {
    PORTB.OUTCLR = PIN_SPI_CS;
    _delay_ms(50);
    PORTB.OUTSET = PIN_SPI_CS;
    _delay_ms(100);
    times--;
  }      
}

// signal the current status with LED blink codes
void signal_status()
{
  switch(device_state)
  {
    case STATE_CAN_FAILED:
      blink_led(2);
      break;
  }
}

void setup_fastled() {
  PORTA.DIRSET = PIN_PWM_A;
  PORTA.OUTCLR = PIN_PWM_A;

  //uint8_t brightness = 0b01010101;
  uint8_t brightness = 255;
  uint16_t index;
  for (index = 0; index < NUM_LEDS; index++) {
    LEDSTRIP.setPixelColor(index, brightness, 0, 0);
  }
  //LEDSTRIP.clear();
  //LEDSTRIP.setBrightness(10);
  //LEDSTRIP.setPixelColor(0, 255, 0, 0);
  LEDSTRIP.show();
}

void led_rainbow(uint16_t offset) {
  uint16_t index;
  #define THIRD (NUM_LEDS/3)

  for (index = 0; index < NUM_LEDS; index++) {
    uint16_t off = (index + offset) % NUM_LEDS;
    uint8_t r = 0, g = 0, b = 0;
    if (off >= 0 && off < THIRD) {
      r = 255 * (THIRD - off) / THIRD;
      g = 255 * off / THIRD;
      b = 0;
    }
    if (off >= THIRD && off < 2*THIRD) {
      r = 0;
      g = 255 * (2*THIRD - off) / THIRD;
      b = 255 * (off - THIRD) / THIRD;
    }
    if (off >= 2*THIRD && off < 3*THIRD) {
      r = 255 * (off - 2*THIRD) / THIRD;
      g = 0;
      b = 255 * (3*THIRD - off) / THIRD;
    }
    LEDSTRIP.setPixelColor(index, r, g, b);
  }
}

int main(void)
{
  // set clock prescaler to 1 o we get 20MHz
  CCP = CCP_IOREG_gc;
  CLKCTRL.MCLKCTRLB = 0;

  PORTB.DIRSET = PIN_SPI_CS;
  PORTB.OUTCLR = PIN_SPI_CS;
  // let the led shine for a bit to show we are online
  _delay_ms(1000);
  PORTB.OUTSET = PIN_SPI_CS;

  // set rtc resolution to 10ms with max error of 5%
  // rtc_init(RTC_RESOLUTION, RTC_TO_FIXED(5l) / 100);
  // rtc_set_callback(rtc_callback);

  // init_temperature();

  // enable interrupts
  sei();

  //if (initialize_can() != CAN_OK)
  //{
    //device_state = STATE_CAN_FAILED;
    //signal_status();
    //return -1;
  //}

  // initialize_pwm(90);
  setup_fastled();

  device_state = STATE_RUNNING;

  // Set sleep mode to STANDBY mode
  //set_sleep_mode(SLEEP_MODE_STANDBY);
  //sleep_enable();
  //sleep_cpu();

  /* Replace with your application code */
  uint16_t offset = 0;
  while (1)
  {
    led_rainbow(offset);
    LEDSTRIP.show();
    _delay_ms(20);
    offset++;
    if (offset >= NUM_LEDS) offset = 0;
  }
}

