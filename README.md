# HeimCtrl devices monorepo

This is part of the HeimCtrl open source home automation project.

Device schematics and firmware


# Device types

Device type ids are kept in ranges
* 20-29: Switching of mains power
* 30-39: Digital inputs to detect switch states
* 40-49: Light control
* 50-59: Motor control for doors


| Device              | Type Id |
|---------------------|---------|
| PowerCtrl Steckdose | 0x10    |
| PowerCtrl Rollo     | 0x11    |
| SwitchCtrl          | 0x20    |
| LightCtrl           | 0x30    |
| LightStripCtrl      | 0x31    |
| DoorCtrl            | 0x40    |

# UserRow data

  uint16_t device_type;
  uint8_t hardware_version;
  device_flags_t flags;
  uint8_t can_bus_id;

  enum device_flags_t: uint32_t
  {
    // a firmware update has been scheduled
    FLAG_DEVICE_UPDATE_SCHEDULED  = 1,
    // if set then we have a server-approved nat id, otherwise we need one
    FLAG_NAT_ID_SET               = 2,
  };
