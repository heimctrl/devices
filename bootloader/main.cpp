/*
* tiny1614_test.cpp
*
* Created: 18/01/2020 12:36:51
* Author : Creator
*/

// oscillator 20MHz and prescaler 6 after boot
#define F_CPU 3200000UL

#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include "main.hpp"
#include "simple_can/simple_can.h"

/*
FUSES = {
  .OSCCFG = FREQSEL_20MHZ_gc, // 0x02
  .SYSCFG0 = CRCSRC_NOCRC_gc | RSTPINCFG_UPDI_gc | FUSE_EESAVE_bm, // 0xF7
  .SYSCFG1 = SUT_64MS_gc, // 0x07
  .APPEND = 0x00, // Application data section disabled
  .BOOTEND = 0x08 // Boot section size = 0x08 * 256 bytes = 512 bytes
};
*/

// 2kb bootloader size
#define PROGRAM_ADDRESS "0x0800"

// PORTA
#define PIN_SPI_SCK   PIN3_bm
#define PIN_SPI_MISO  PIN2_bm
#define PIN_SPI_MOSI  PIN1_bm
// PORTB
#define PIN_SPI_CS    PIN3_bm


uint8_t initialize_can()
{
  uint8_t res;
  uint8_t index;

  // initialize CAN bus
  res = can_begin();
  if (res != CAN_OK)
  {
    return res;
  }

  uint8_t msg[8] = { 0xAA, 0xBB };
  res = sendMsgBuf(1, 2, msg);
  if (res != CAN_OK)
  {
    return res;
  }

  // wait for the data to be read again
  uint32_t id;
  uint8_t len;
  for(index = 0; index < 250; index++)
  {
    res = readMsgBuf(&id, &len, msg);
    if (res != CAN_NOMSG) break;
  }
  
  // test went ok, switch to normal mode
  if (res == CAN_OK)
  {
    if (id != 1 || len != 2 || msg[0] != 0xAA || msg[1] != 0xBB)
    {
      res = CAN_FAIL;
    }
    else
    {
      res = mcp2515_setCANCTRL_Mode(MCP_NORMAL);
    }
  }

  return res;
}

void blink_led(uint8_t times)
{
  // LED off
  PORTB.OUTSET = PIN_SPI_CS;

  while (times > 0)
  {
    // LED on
    PORTB.OUTCLR = PIN_SPI_CS;
    _delay_ms(50);
    // LED off
    PORTB.OUTSET = PIN_SPI_CS;
    _delay_ms(200);
    times--;
  }
}

void fail_boot(uint8_t code) {
  while(1)
  {
    blink_led(code);
    _delay_ms(1000);
  }
}

void write_page()
{
  NVMCTRL.CTRLA = NVMCTRL_CMD_PAGEERASEWRITE_gc;
  
  while(NVMCTRL.STATUS | NVMCTRL_FBUSY_bm);
  
}

void send_fragmented(uint8_t* msg, uint8_t len)
{
  uint8_t chunk[8];

  // send the fragment header first
  sendMsgBuf(CAN_ID(0b10, 1, 0x00, 0x02), 2, msg);
}

void send_start_info()
{
  uint32_t serial_num = *((uint32_t*) &SIGROW.SERNUM0);
}

// we are expecting a firmware update so wait for it for some time
void receive_firmware()
{
/*
  uint8_t res;
  uint8_t index;

  // initialize CAN bus
  res = can_begin();
  if (res != CAN_OK)
  {
    blink_led(2);
    return res;
  }

  // poll for the update initiation message
  for(index = 0; index < 250; index++)
  {
    do {
      res = readMsgBuf(&id, &len, msg);
    } 
    if (res != CAN_NOMSG) break;
  }

  uint8_t msg[8] = { 0xAA, 0xBB };
  res = sendMsgBuf(1, 2, msg);
  if (res != CAN_OK)
  {
    return res;
  }

  // wait for the data to be read again
  uint32_t id;
  uint8_t len;
  for(index = 0; index < 250; index++)
  {
    res = readMsgBuf(&id, &len, msg);
    if (res != CAN_NOMSG) break;
  }
  
  // test went ok, switch to normal mode
  if (res == CAN_OK)
  {
    if (id != 1 || len != 2 || msg[0] != 0xAA || msg[1] != 0xBB)
    {
      res = CAN_FAIL;
    }
    else
    {
      res = mcp2515_setCANCTRL_Mode(MCP_NORMAL);
    }
  }
  if (initialize_can() != CAN_OK)
  {
    fail_boot(2);
    return;
  }


  can_end();
*/
}

__attribute__((naked)) __attribute__((section(".ctors"))) void boot(void)
{
  /* Initialize system for C support */
  asm volatile("clr r1");

  PORTB.DIRSET = PIN_SPI_CS;

  // device is uninitialized
  if (ur_device_settings.device_type == 0xFFFF)
  {
    fail_boot(3);
  }

  // update pending
  if (ur_device_settings.flags | DEVICE_UPDATE_SCHEDULED)
  {
    receive_firmware();
  }

  // jump to user program
  asm("jmp " PROGRAM_ADDRESS);
}
