/*
  mcp_can.h
  2012 Copyright (c) Seeed Technology Inc.  All right reserved.
  2017 Copyright (c) Cory J. Fowler  All Rights Reserved.

  Author:Loovee
  Contributor: Cory J. Fowler
  2017-09-25
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-
  1301  USA
*/
#ifndef _MCP2515_H_
#define _MCP2515_H_

#include <avr/io.h>
#include "simple_can_dfs.h"

#define MAX_CHAR_IN_MESSAGE 8

/*
10000.0ddlnnnn.nnnn0000.cccccccc
  cccccccc contains the command byte.
  nnnnnnnn is the NAT (0x01�0x7E), which is the id of the extension on the Loxone Link bus. A NAT of 0xFF is a broadcast to all extensions. Bit 7 is set for parked extensions, which have been detected by the Miniserver, but are not configured in Loxone Config.
  dd is a direction/type of the command: 00 = sent from a extension/device, 10 = from a extension/device, when sending a shortcut messages back to the Miniserver, 11 = sent from the Miniserver
  l package type: 0 = regular package, 1 = fragmented package
*/
#define CAN_ID(dir, pkg, nat, cmd) (uint32_t)((0x10000 << 24) + (((uint32_t)dir) << 21) + (((uint32_t)pkg) << 20) + (((uint32_t)nat) << 12) + (cmd & 0xFF))

// PORTA
#define PIN_SPI_SCK   PIN3_bm
#define PIN_SPI_MISO  PIN2_bm
#define PIN_SPI_MOSI  PIN1_bm
// PORTB
#define PIN_SPI_CS    PIN3_bm

typedef struct can_speed_cfg_t
{
  uint8_t cfg1, cfg2, cfg3;
} can_speed_cfg;

/*********************************************************************************************************
 *  SPI driver function 
 *********************************************************************************************************/

uint8_t SPI_transmit(uint8_t value);
uint8_t SPI_receive(void);

void mcp2515_select(void);
void mcp2515_unselect(void);

/*********************************************************************************************************
 *  mcp2515 driver function 
 *********************************************************************************************************/

void mcp2515_reset(void);                                           // Soft Reset MCP2515

uint8_t mcp2515_readRegister(const uint8_t address);                // Read MCP2515 register
    
void mcp2515_readRegisters(const uint8_t address,                   // Read MCP2515 successive registers
	                          uint8_t values[], 
                            const uint8_t n);
   
void mcp2515_setRegister(const uint8_t address,                     // Set MCP2515 register
                          const uint8_t value);

void mcp2515_setRegisters(const uint8_t address,                    // Set MCP2515 successive registers
                          const uint8_t values[],
                          const uint8_t n);
    
void mcp2515_initCANBuffers(void);
    
void mcp2515_modifyRegister(const uint8_t address,                  // Set specific bit(s) of a register
                            const uint8_t mask,
                            const uint8_t data);

uint8_t mcp2515_readStatus(void);                                   // Read MCP2515 Status
uint8_t mcp2515_setCANCTRL_Mode(const uint8_t newmode);             // Set mode

uint8_t mcp2515_init(void);                                         // Initialize Controller
		       			       
void mcp2515_write_id(const uint8_t mcp_addr,                       // Write CAN ID
                      const uint8_t ext,
                      const uint32_t id );

void mcp2515_read_id(const uint8_t mcp_addr,                        // Read CAN ID
				              uint8_t* ext,
                      uint32_t* id );

void mcp2515_write_canMsg(const uint8_t buffer_sidh_addr );         // Write CAN message
void mcp2515_read_canMsg(const uint8_t buffer_sidh_addr);           // Read CAN message
uint8_t mcp2515_getNextFreeTXBuf(uint8_t *txbuf_n);                 // Find empty transmit buffer

/*********************************************************************************************************
 *  CAN operator function
 *********************************************************************************************************/

uint8_t sendMsg();                                                  // Send message

/*********************************************************************************************************
 *  Transactions and interrupt management
 *********************************************************************************************************/

uint8_t can_begin(void);                                            // Initialize controller parameters
void can_end(void);                                                 // Reset can controller and disable spi
uint8_t sendMsgBuf(uint32_t id, uint8_t len, uint8_t *buf);         // Send message to transmit buffer
uint8_t readMsgBuf(uint32_t *id, uint8_t *len, uint8_t *buf);       // Read message from receive buffer

#endif
/*********************************************************************************************************
 *  END FILE
 *********************************************************************************************************/
