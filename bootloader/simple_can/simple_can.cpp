/*
simple_can.cpp
2012 Copyright (c) Seeed Technology Inc.  All right reserved.
2017 Copyright (c) Cory J. Fowler  All Rights Reserved.

Author: Loovee
Contributor: Cory J. Fowler
2017-09-25

Converted for attiny and simplified for bootloader by: Jens Elstner
Date: 2021-02-14

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-
1301  USA
*/
#include "simple_can.h"

uint8_t     m_nExtFlg;                        // Identifier Type
                                              // Extended (29 bit) or Standard (11 bit)
uint32_t    m_nID;                            // CAN ID
uint8_t     m_nDlc;                           // Data Length Code
uint8_t     m_nDta[MAX_CHAR_IN_MESSAGE];      // Data array
uint8_t     m_nRtr;                           // Remote request flag

/*********************************************************************************************************
** USED
** Function name:           can_begin
** Descriptions:            Public function to declare controller initialization parameters.
*********************************************************************************************************/
uint8_t can_begin(void)
{
  uint8_t res;

  PORTA.DIRSET = PIN_SPI_SCK | PIN_SPI_MOSI;
  PORTA.DIRCLR = PIN_SPI_MISO;
  PORTB.DIRSET = PIN_SPI_CS;
  PORTB.OUTSET = PIN_SPI_CS;

  SPI0.CTRLA = SPI_MASTER_bm
  | SPI_PRESC_DIV64_gc;
  // | SPI_CLK2X_bm;
  SPI0.CTRLB = SPI_MODE_0_gc
  | SPI_SSD_bm;
  // SPI0.INTCTRL = SPI_IE_bm | SPI_TXCIE_bm | SPI_RXCIE_bm;
  SPI0.CTRLA |= SPI_ENABLE_bm;
  mcp2515_unselect();

  res = mcp2515_init();
  if (res == CAN_OK)
  {
    return CAN_OK;
  }
  return CAN_FAILINIT;}

/*********************************************************************************************************
** USED
** Function name:           can_end
** Descriptions:            Public function to close communication with the CAN controller
*********************************************************************************************************/
void can_end(void)
{
  mcp2515_reset();
  SPI0.CTRLA &= ~SPI_ENABLE_bm;}

/*********************************************************************************************************
** USED
** Function name:           SPI_transmit
** Descriptions:            Transmit a byte on the SPI bus
*********************************************************************************************************/
__attribute__ ((noinline)) uint8_t SPI_transmit(uint8_t value)
{
  while(SPI0.INTFLAGS & SPI_IF_bm);
  SPI0.DATA = value;
  while(!(SPI0.INTFLAGS & SPI_IF_bm));
  return SPI0.DATA;}

/*********************************************************************************************************
** USED
** Function name:           SPI_receive
** Descriptions:            Read a bit from the SPI bus
*********************************************************************************************************/
__attribute__ ((noinline)) uint8_t SPI_receive()
{
  while(SPI0.INTFLAGS & SPI_IF_bm);
  SPI0.DATA = 0x00;
  while(!(SPI0.INTFLAGS & SPI_IF_bm));
  return SPI0.DATA;}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_select
** Descriptions:            Select the MCP device on SPI
*********************************************************************************************************/
__attribute__ ((noinline)) void mcp2515_select(void)
{
  PORTB.OUTCLR = PIN_SPI_CS;}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_unselect
** Descriptions:            Unselect the MCP device on SPI
*********************************************************************************************************/
__attribute__ ((noinline)) void mcp2515_unselect(void)
{
  PORTB.OUTSET = PIN_SPI_CS;}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_reset
** Descriptions:            Performs a software reset
*********************************************************************************************************/
void mcp2515_reset(void)
{
  uint16_t index;

  mcp2515_select();
  SPI_transmit(MCP_RESET);
  mcp2515_unselect();
  // simple short delay
  for (index = 0; index < 1000; index++);
}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_readRegister
** Descriptions:            Read data register
*********************************************************************************************************/
__attribute__ ((noinline)) uint8_t mcp2515_readRegister(const uint8_t address)
{
  uint8_t ret;

  mcp2515_select();
  SPI_transmit(MCP_READ);
  SPI_transmit(address);
  ret = SPI_receive();
  mcp2515_unselect();

  return ret;}

/*********************************************************************************************************
** Function name:           mcp2515_readRegisterS
** Descriptions:            Reads successive data registers
*********************************************************************************************************/
__attribute__ ((noinline)) void mcp2515_readRegisters(const uint8_t address, uint8_t values[], const uint8_t numValues)
{
  uint8_t index;

  mcp2515_select();
  SPI_transmit(MCP_READ);
  SPI_transmit(address);

  // mcp2515 has auto-increment of address-pointer
  for (index = 0; index < numValues; index++)
  {
    values[index] = SPI_receive();
  }

  mcp2515_unselect();
}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_setRegister
** Descriptions:            Sets data register
*********************************************************************************************************/
__attribute__ ((noinline)) void mcp2515_setRegister(const uint8_t address, const uint8_t value)
{
  mcp2515_select();
  SPI_transmit(MCP_WRITE);
  SPI_transmit(address);
  SPI_transmit(value);
  mcp2515_unselect();
}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_setRegisterS
** Descriptions:            Sets successive data registers
*********************************************************************************************************/
__attribute__ ((noinline)) void mcp2515_setRegisters(const uint8_t address, const uint8_t values[], const uint8_t n)
{
  uint8_t index;

  mcp2515_select();
  SPI_transmit(MCP_WRITE);
  SPI_transmit(address);
  
  for (index = 0; index < n; index++) SPI_transmit(values[index]);
  
  mcp2515_unselect();
}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_modifyRegister
** Descriptions:            Sets specific bits of a register
*********************************************************************************************************/
__attribute__ ((noinline)) void mcp2515_modifyRegister(const uint8_t address, const uint8_t mask, const uint8_t data)
{
  mcp2515_select();
  SPI_transmit(MCP_BITMOD);
  SPI_transmit(address);
  SPI_transmit(mask);
  SPI_transmit(data);
  mcp2515_unselect();
}

/*********************************************************************************************************
** Function name:           mcp2515_readStatus
** Descriptions:            Reads status register
*********************************************************************************************************/
uint8_t mcp2515_readStatus(void)
{
  uint8_t data;

  mcp2515_select();
  SPI_transmit(MCP_READ_STATUS);
  data = SPI_receive();
  mcp2515_unselect();

  return data;}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_setCANCTRL_Mode
** Descriptions:            Set control mode
*********************************************************************************************************/
__attribute__ ((noinline)) uint8_t mcp2515_setCANCTRL_Mode(const uint8_t mode)
{
  uint8_t curMode;

  mcp2515_modifyRegister(MCP_CANCTRL, MODE_MASK, mode);

  curMode = mcp2515_readRegister(MCP_CANCTRL);
  curMode &= MODE_MASK;

  if (curMode == mode)
  {
    return CAN_OK;
  }
  return CAN_FAIL;}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_initCANBuffers
** Descriptions:            Initialize Buffers, Masks, and Filters
*********************************************************************************************************/
void mcp2515_initCANBuffers(void)
{
  uint8_t i, a1, a2, a3;
  
  //uint8_t std = 0;
  //uint8_t ext = 1;
  //uint32_t ulMask = 0x00, ulFilt = 0x00;

  //mcp2515_write_mf(MCP_RXM0SIDH, ext, ulMask);			/*Set both masks to 0           */
  //mcp2515_write_mf(MCP_RXM1SIDH, ext, ulMask);			/*Mask register ignores ext bit */
  
  /* Set all filters to 0         */
  //mcp2515_write_mf(MCP_RXF0SIDH, ext, ulFilt);			/* RXB0: extended               */
  //mcp2515_write_mf(MCP_RXF1SIDH, std, ulFilt);			/* RXB1: standard               */
  //mcp2515_write_mf(MCP_RXF2SIDH, ext, ulFilt);			/* RXB2: extended               */
  //mcp2515_write_mf(MCP_RXF3SIDH, std, ulFilt);			/* RXB3: standard               */
  //mcp2515_write_mf(MCP_RXF4SIDH, ext, ulFilt);
  //mcp2515_write_mf(MCP_RXF5SIDH, std, ulFilt);

  /* Clear, deactivate the three  */
  /* transmit buffers             */
  /* TXBnCTRL -> TXBnD7           */
  a1 = MCP_TXB0CTRL;
  a2 = MCP_TXB1CTRL;
  a3 = MCP_TXB2CTRL;
  for (i = 0; i < 14; i++)
  {                                          /* in-buffer loop               */
    mcp2515_setRegister(a1, 0);
    mcp2515_setRegister(a2, 0);
    mcp2515_setRegister(a3, 0);
    a1++;
    a2++;
    a3++;
  }
  // turn masks and filters off
  mcp2515_setRegister(MCP_RXB0CTRL, 0x60);
  mcp2515_setRegister(MCP_RXB1CTRL, 0x60);}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_init
** Descriptions:            Initialize the controller
*********************************************************************************************************/
uint8_t mcp2515_init(void)
{
  uint8_t res;

  mcp2515_reset();
  
  res = mcp2515_setCANCTRL_Mode(MODE_CONFIG);
  if(res != CAN_OK)
  {
    return res;
  }

  // Set Baud rate
  // res = mcp2515_configRate();
  mcp2515_setRegister(MCP_CNF1, MCP_8MHz_500kBPS_CFG1);
  mcp2515_setRegister(MCP_CNF2, MCP_8MHz_500kBPS_CFG2);
  mcp2515_setRegister(MCP_CNF3, MCP_8MHz_500kBPS_CFG3);

  // initialize can buffers
  mcp2515_initCANBuffers();

  // no interrupts needed
  // interrupt mode
  // disable rx buffer interrupts
  mcp2515_setRegister(MCP_CANINTE, 0);
  // clear all current interrupt flags
  mcp2515_setRegister(MCP_CANINTF, 0);

  //Sets BF pins as GPO
  mcp2515_setRegister(MCP_BFPCTRL, MCP_BxBFS_MASK | MCP_BxBFE_MASK);
  //Sets RTS pins as GPI
  mcp2515_setRegister(MCP_TXRTSCTRL, 0x00);

  // canIDMode == MCP_ANY
  mcp2515_modifyRegister(MCP_RXB0CTRL, MCP_RXB_RX_MASK | MCP_RXB_BUKT_MASK, MCP_RXB_RX_ANY | MCP_RXB_BUKT_MASK);
  mcp2515_modifyRegister(MCP_RXB1CTRL, MCP_RXB_RX_MASK, MCP_RXB_RX_ANY);

  res = mcp2515_setCANCTRL_Mode(MCP_LOOPBACK);

  return res;}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_write_id
** Descriptions:            Write CAN ID
*********************************************************************************************************/
void mcp2515_write_id( const uint8_t mcp_addr, const uint8_t ext, const uint32_t id )
{
  uint16_t canid;
  uint8_t tbufdata[4];

  canid = (uint16_t)(id & 0x0FFFF);

  if (ext == 1)
  {
    tbufdata[MCP_EID0] = (uint8_t) (canid & 0xFF);
    tbufdata[MCP_EID8] = (uint8_t) (canid >> 8);
    canid = (uint16_t)(id >> 16);
    tbufdata[MCP_SIDL] = (uint8_t) (canid & 0x03);
    tbufdata[MCP_SIDL] += (uint8_t) ((canid & 0x1C) << 3);
    tbufdata[MCP_SIDL] |= MCP_TXB_EXIDE_M;
    tbufdata[MCP_SIDH] = (uint8_t) (canid >> 5 );
  }
  else
  {
    tbufdata[MCP_SIDH] = (uint8_t) (canid >> 3 );
    tbufdata[MCP_SIDL] = (uint8_t) ((canid & 0x07 ) << 5);
    tbufdata[MCP_EID0] = 0;
    tbufdata[MCP_EID8] = 0;
  }
  
  mcp2515_setRegisters( mcp_addr, tbufdata, 4 );}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_read_id
** Descriptions:            Read CAN ID
*********************************************************************************************************/
void mcp2515_read_id( const uint8_t mcp_addr, uint8_t* ext, uint32_t* id )
{
  uint8_t tbufdata[4];

  *ext = 0;
  *id = 0;

  mcp2515_readRegisters( mcp_addr, tbufdata, 4 );

  *id = (tbufdata[MCP_SIDH]<<3) + (tbufdata[MCP_SIDL]>>5);

  if ( (tbufdata[MCP_SIDL] & MCP_TXB_EXIDE_M) ==  MCP_TXB_EXIDE_M )
  {
    /* extended id                  */
    *id = (*id<<2) + (tbufdata[MCP_SIDL] & 0x03);
    *id = (*id<<8) + tbufdata[MCP_EID8];
    *id = (*id<<8) + tbufdata[MCP_EID0];
    *ext = 1;
  }}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_write_canMsg
** Descriptions:            Write message
*********************************************************************************************************/
void mcp2515_write_canMsg(const uint8_t buffer_sidh_addr)
{
  uint8_t mcp_addr;
  mcp_addr = buffer_sidh_addr;
  mcp2515_setRegisters(mcp_addr + 5, m_nDta, m_nDlc);                 /* write data bytes             */
  
  if (m_nRtr == 1)                                                    /* if RTR set bit in byte       */
  {
    m_nDlc |= MCP_RTR_MASK;
  }

  mcp2515_setRegister((mcp_addr+4), m_nDlc);                          /* write the RTR and DLC        */
  mcp2515_write_id(mcp_addr, m_nExtFlg, m_nID);                       /* write CAN id                 */
}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_read_canMsg
** Descriptions:            Read message
*********************************************************************************************************/
void mcp2515_read_canMsg( const uint8_t buffer_sidh_addr)        /* read can msg                 */
{
  uint8_t mcp_addr, ctrl;

  mcp_addr = buffer_sidh_addr;

  mcp2515_read_id(mcp_addr, &m_nExtFlg, &m_nID);

  ctrl = mcp2515_readRegister(mcp_addr - 1);
  m_nDlc = mcp2515_readRegister(mcp_addr + 4);

  if (ctrl & 0x08)
  {
    m_nRtr = 1;
  }
  else
  {
    m_nRtr = 0;
  }

  m_nDlc &= MCP_DLC_MASK;
  mcp2515_readRegisters(mcp_addr + 5, &(m_nDta[0]), m_nDlc);}

/*********************************************************************************************************
** USED
** Function name:           mcp2515_getNextFreeTXBuf
** Descriptions:            Send message
*********************************************************************************************************/
uint8_t mcp2515_getNextFreeTXBuf(uint8_t *txbuf_n)                 /* get Next free txbuf          */
{
  uint8_t res, i, ctrlval;
  uint8_t ctrlregs[MCP_N_TXBUFFERS] = { MCP_TXB0CTRL, MCP_TXB1CTRL, MCP_TXB2CTRL };

  res = MCP_ALLTXBUSY;
  *txbuf_n = 0x00;

  /* check all 3 TX-Buffers       */
  for (i=0; i<MCP_N_TXBUFFERS; i++)
  {
    ctrlval = mcp2515_readRegister(ctrlregs[i]);
    if ((ctrlval & MCP_TXB_TXREQ_M) == 0)
    {
      *txbuf_n = ctrlregs[i]+1;                                   /* return SIDH-address of Buffer*/
      
      res = CAN_OK;
      return res;                                                 /* ! function exit              */
    }
  }
  return res;}

/*********************************************************************************************************
** Function name:           sendMsg
** Descriptions:            Send message
*********************************************************************************************************/
uint8_t sendMsg()
{
  uint8_t res, res1, txbuf_n;
  uint16_t uiTimeOut = 0;

  do
  {
    res = mcp2515_getNextFreeTXBuf(&txbuf_n);                       /* info = addr.                 */
    uiTimeOut++;
  } while (res == MCP_ALLTXBUSY && (uiTimeOut < TIMEOUTVALUE));

  if(uiTimeOut == TIMEOUTVALUE)
  {
    return CAN_GETTXBFTIMEOUT;                                      /* get tx buff time out         */
  }
  uiTimeOut = 0;
  mcp2515_write_canMsg(txbuf_n);
  mcp2515_modifyRegister(txbuf_n - 1, MCP_TXB_TXREQ_M, MCP_TXB_TXREQ_M);
  
  do
  {
    uiTimeOut++;
    res1 = mcp2515_readRegister(txbuf_n - 1);                        /* read send buff ctrl reg 	*/
    res1 = res1 & 0x08;
  } while (res1 && (uiTimeOut < TIMEOUTVALUE));
  
  if(uiTimeOut == TIMEOUTVALUE)                                      /* send msg timeout             */
  {
    return CAN_SENDMSGTIMEOUT;
  }

  return CAN_OK;}

/*********************************************************************************************************
** Function name:           sendMsgBuf
** Descriptions:            Send message to transmit buffer
*********************************************************************************************************/
uint8_t sendMsgBuf(uint32_t id, uint8_t len, uint8_t *buf)
{
  uint8_t ext = 0, rtr = 0;
  uint8_t res;

  if((id & 0x80000000) == 0x80000000)
  {
    ext = 1;
  }
  if((id & 0x40000000) == 0x40000000)
  {
    rtr = 1;
  }

  // setMsg(id, rtr, ext, len, buf);
  m_nID     = id;
  m_nRtr    = rtr;
  m_nExtFlg = ext;
  m_nDlc    = len;
  for(int index = 0; index < MAX_CHAR_IN_MESSAGE; index++)
  {
    m_nDta[index] = *(buf + index);
  }

  res = sendMsg();

  return res;}

/*********************************************************************************************************
** USED
** Function name:           readMsgBuf
** Descriptions:            Public function, Reads message from receive buffer.
*********************************************************************************************************/
uint8_t readMsgBuf(uint32_t *id, uint8_t *len, uint8_t buf[])
{
  uint8_t stat;

  // if(readMsg() == CAN_NOMSG)
  stat = mcp2515_readStatus();

  if (stat & MCP_STAT_RX0IF)                                        /* Msg in Buffer 0              */
  {
    mcp2515_read_canMsg(MCP_RXBUF_0);
    mcp2515_modifyRegister(MCP_CANINTF, MCP_RX0IF, 0);
  }
  else if (stat & MCP_STAT_RX1IF)                                   /* Msg in Buffer 1              */
  {
    mcp2515_read_canMsg(MCP_RXBUF_1);
    mcp2515_modifyRegister(MCP_CANINTF, MCP_RX1IF, 0);
  }
  else
  {
    return CAN_NOMSG;
  }

  if (m_nExtFlg)
  {
    // this is wasting a lot of code bytes so we try to process only top byte
    // m_nID |= 0x80000000;
    ((uint8_t*)m_nID)[0] |= 0x80;
  }
  if (m_nRtr)
  {
    // this is wasting a lot of code bytes so we try to process only top byte
    // m_nID |= 0x40000000;
    ((uint8_t*)m_nID)[0] |= 0x40;
  }
  *id  = m_nID;
  *len = m_nDlc;
  
  for(int index = 0; index < m_nDlc; index++)
  {
    buf[index] = m_nDta[index];
  }
  return CAN_OK;}

/*********************************************************************************************************
END FILE
*********************************************************************************************************/
