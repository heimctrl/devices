#ifndef _MAIN_HPP_
#define _MAIN_HPP_

#include <avr/io.h>

// USERROW helpers
// .userrow section is defined in linker settings
// eeprom_write and eeprom_read functions expect eeprom to be at 0x1400 and expect addresses to start from 0
#define URMEM __attribute((section(".userrow")))
#define URWRITE(variable, value)  eeprom_write_byte(&variable - 0x1400, value)

enum device_flags_t: uint32_t
{
  DEVICE_UPDATE_SCHEDULED   = 1,
};

// USERROW data
struct device_settings_t {
  uint16_t device_type;
  uint8_t hardware_version;
  device_flags_t flags;
  uint8_t can_bus_id;
};
URMEM device_settings_t ur_device_settings;

// set clock prescaler to 1 o we get 20MHz
#define ENABLE_20MHZ CCP = CCP_IOREG_gc; CLKCTRL.MCLKCTRLB = 0;

// macros to set the interrupt vector table to either bootloader or app
#define SET_IVT_BOOT CCP = CCP_IOREG_gc; CPUINT.CTRLA |= CPUINT_IVSEL_bm;
#define SET_IVT_APP CCP = CCP_IOREG_gc; CPUINT.CTRLA &= ~CPUINT_IVSEL_bm;

#endif
/*********************************************************************************************************
 *  END FILE
 *********************************************************************************************************/
