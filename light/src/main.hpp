#ifndef _MAIN_HPP_
#define _MAIN_HPP_

#include "mcp_can/protocol.hpp"

// set clock prescaler to 1 so we get 20MHz
#define ENABLE_20MHZ CCP = CCP_IOREG_gc; CLKCTRL.MCLKCTRLB = 0;

// command 0x80, receive the current value
#define VALUE_VERSION  1
struct MsgValuePWMDimmer_t
{
  uint8_t version;          // version of the message
  uint8_t channel_a_level;  // brightness of channel A in % (0-100)
  uint8_t channel_b_level;  // brightness of channel B in % (0-100)
};

#define CONFIG_VERSION  1
struct MsgConfigPWMDimmerV1_t
{
  uint8_t channel_a_level_default;  // brightness of channel A in % after restart (0-101), 101% - retain last value
  uint8_t channel_a_fade_rate;      // how quickly to fade brightness changes (0.1 seconds per 100%)
  uint8_t channel_b_level_default;  // brightness of channel B in % after restart (0-101), 101% - retain last value
  uint8_t channel_b_fade_rate;      // how quickly to fade brightness changes (0.1 seconds per 100%)
};

MsgConfigPWMDimmerV1_t* device_config = (MsgConfigPWMDimmerV1_t*)&USERROW.USERROW12;
#define DEVICE_CONFIG_ADDR  (0xFF00 + (uint8_t*)device_config - &USERROW.USERROW0)

struct CurrentValue_t
{
  uint8_t channel_a_level;  // brightness of channel A in % (0-100)
  uint8_t channel_b_level;  // brightness of channel B in % (0-100)
};

CurrentValue_t current_value;

CurrentValue_t t_value;
CurrentValue_t* target_value = &t_value;
// CurrentValue_t* target_value = (CurrentValue_t*)&USERROW.USERROW26;
// #define DEVICE_VALUE_ADDR  (0xFF00 + (uint8_t*)target_value - &USERROW.USERROW0)

#endif
/*********************************************************************************************************
 *  END FILE
 *********************************************************************************************************/
