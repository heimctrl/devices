/*
* main.cpp
*
* Created: 18/01/2020 12:36:51
* Author : Creator
*/
// #define F_CPU 3200000UL
#define F_CPU 20000000UL

#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include "main.hpp"
#include "temperature.hpp"
#include "rtc/rtc.hpp"

// firmware version: major.minor
#define FIRMWARE_VERSION 0x0001

#define SECS_TO_RTC(secs) (secs * 1024)
// number of timer ticks to change brightness by 1
#define TICKS_PER_LEVEL(fade_rate)  (SECS_TO_RTC(fade_rate) / 100)

// PORTA
#define PIN_PWM_A       PIN4_bm
#define PIN_PWM_B       PIN5_bm
// PORTB
#define PIN_CAN_INT     PIN2_bm
#define PIN_MOTOR_STBY  PIN1_bm

bool pwm_ready = false;
uint8_t channel_a_cur = 0;        // current pwm percentage
uint16_t channel_a_ticks = 0;     // current tick count
uint16_t channel_a_target = 0;    // tick count target
uint8_t channel_b_cur = 0;        // current pwm percentage
uint16_t channel_b_ticks = 0;     // current tick count
uint16_t channel_b_target = 0;    // tick count target

// signal the current status with LED blink codes
void signal_status();

/************************************************************************/
/* Send a digital value                                                 */
/************************************************************************/
uint8_t send_target_value()
{
  MsgValuePWMDimmer_t msg;
  msg.version = VALUE_VERSION;
  msg.channel_a_level = target_value->channel_a_level;
  msg.channel_b_level = target_value->channel_b_level;
  return send_message(CMD_DIGITAL_VALUE, (uint8_t*)&msg, sizeof(MsgValuePWMDimmer_t));
}

void handleCanMessage(uint8_t cmd, uint8_t* msg, uint16_t size)
{
  switch(cmd)
  {
    case CMD_SEND_CONFIG:
      {
        // received configuration, check size and store in userrow
        MsgConfigHead_t* msg_config = (MsgConfigHead_t*)msg;
        if (msg_config->size == sizeof(MsgConfigPWMDimmerV1_t) && msg_config->version == CONFIG_VERSION)
        {
          eeprom_busy_wait();
          eeprom_update_block(msg_config->data, (void*)DEVICE_CONFIG_ADDR, sizeof(MsgConfigPWMDimmerV1_t));

          // set target ticks for 1% change in brightness
          channel_a_target = device_config->channel_a_fade_rate;
          channel_b_target = device_config->channel_b_fade_rate;
        }
      }
      break;
    case CMD_DIGITAL_VALUE:
      {
        // received a value, check size and store in userrow
          MsgValuePWMDimmer_t* msg_value = (MsgValuePWMDimmer_t*)msg;
        if (size == sizeof(MsgValuePWMDimmer_t) && msg_value->version == VALUE_VERSION)
        {
          CurrentValue_t* msg_cur = (CurrentValue_t*)(msg + 1);
          if (msg_cur->channel_a_level > 100) msg_cur->channel_a_level = 100;
          if (msg_cur->channel_b_level > 100) msg_cur->channel_b_level = 100;

          target_value->channel_a_level = msg_cur->channel_a_level;
          target_value->channel_b_level = msg_cur->channel_b_level;

          // eeprom_busy_wait();
          // eeprom_update_block(msg_cur, (void*)DEVICE_VALUE_ADDR, sizeof(CurrentValue_t));

          // acknowledge receipt of the new value to the server
          send_target_value();
        }
      }
      break;
  }
}

ISR(PORTB_PORT_vect)
{
  uint8_t int_flags = PORTB.INTFLAGS;
  PORTB.INTFLAGS |= 0xFF;
  if ((int_flags & PIN_CAN_INT) != 0)
  {
    PORTB.INTFLAGS |= PIN_CAN_INT;
    handleCanInterrupt();
  }
}


uint16_t pwm_clr;
void initialize_pwm(uint16_t initial = 100)
{
  // enable pwm on first output
  PORTA.DIRSET = PIN_PWM_A | PIN_PWM_B | PIN6_bm | PIN7_bm;

  PORTB.DIRSET = PIN_MOTOR_STBY;
  PORTB.OUTSET = PIN_MOTOR_STBY;

  CCP = CCP_IOREG_gc;
  TCD0.FAULTCTRL = TCD_CMPAEN_bm | TCD_CMPBEN_bm;
  TCD0.DBGCTRL = TCD_DBGRUN_bm;

  TCD0.CTRLB = TCD_WGMODE_DS_gc;
  pwm_clr = 20000000 / (1 * 2 * 100000) - 1;
  TCD0.CMPACLR = pwm_clr;
  TCD0.CMPBCLR = pwm_clr;
  TCD0.CMPASET = initial * pwm_clr / 100;
  TCD0.CMPBSET = initial * pwm_clr / 100;

  while(!(TCD0.STATUS & TCD_ENRDY_bm));
  TCD0.CTRLA = TCD_CLKSEL_20MHZ_gc | TCD_CNTPRES_DIV1_gc | TCD_ENABLE_bm;
  pwm_ready = true;
}

void set_pwm_a(uint16_t value = 100)
{
  while(!(TCD0.STATUS & TCD_CMDRDY_bm));
  TCD0.CMPASET = value * pwm_clr / 100;
  TCD0.CTRLE = TCD_SYNCEOC_bm;
}

void set_pwm_b(uint16_t value = 100)
{
  while(!(TCD0.STATUS & TCD_CMDRDY_bm));
  TCD0.CMPBSET = value * pwm_clr / 100;
  TCD0.CTRLE = TCD_SYNCEOC_bm;
}

void set_pwm_to_values(CurrentValue_t* value) {
  if (value->channel_a_level != 0 || value->channel_b_level != 0) {
    // enable PWM if one is non-zero
    TCD0.CTRLA |= TCD_ENABLE_bm;
  }

  set_pwm_a(value->channel_a_level);
  set_pwm_b(value->channel_b_level);

  if (value->channel_a_level == 0 && value->channel_b_level == 0) {
    // disable PWM if both are 0
    TCD0.CTRLA &= ~TCD_ENABLE_bm;
  }
}

uint16_t temp_delay = 0;
void rtc_callback()
{
  // wdt_reset();

  handleTimer(1);

  if (device_state != STATE_RUNNING)
  {
    // signal_status();
    return;
  }

  //if (temp_delay <= 0)
  //{
    //temp_delay = SECS_TO_RTC(3);
    //read_temperature();
  //}
  //temp_delay--;
  
  if (pwm_ready) {
    bool value_changed = false;

    if (target_value->channel_a_level != current_value.channel_a_level) {
      channel_a_ticks++;
      if (channel_a_ticks >= channel_a_target) {
        channel_a_ticks = 0;
        if (current_value.channel_a_level < target_value->channel_a_level) current_value.channel_a_level++;
        else current_value.channel_a_level--;
        value_changed = true;
      }
    }

    if (target_value->channel_b_level != current_value.channel_b_level) {
      channel_b_ticks++;
      if (channel_b_ticks >= channel_b_target) {
        channel_b_ticks = 0;
        if (current_value.channel_b_level < target_value->channel_b_level) current_value.channel_b_level++;
        else current_value.channel_b_level--;
        value_changed = true;
      }
    }

    if (value_changed) {
      set_pwm_to_values(&current_value);
    }
  }  
}

void blink_led(int8_t times)
{
  while (times > 0)
  {
    PORTB.OUTCLR = PIN_SPI_CS;
    _delay_ms(50);
    PORTB.OUTSET = PIN_SPI_CS;
    _delay_ms(100);
    times--;
  }
}

// signal the current status with LED blink codes
void signal_status()
{
  switch(device_state)
  {
    case STATE_CAN_FAILED:
      blink_led(2);
      break;
    default:
      break;
  }
}

int main(void)
{
  ENABLE_20MHZ

  PORTB.DIRSET = PIN_SPI_CS;
  PORTB.OUTCLR = PIN_SPI_CS;

  if (!check_device_type(DEV_LIGHTCTRL))
  {
    // keep led on
    PORTB.OUTCLR = PIN_SPI_CS;
    return -1;
  }

  // set rtc resolution to 10ms with max error of 5%
  // rtc_init(10, RTC_TO_FIXED(5l) / 100);
  rtc_init_1ms();
  rtc_set_callback(rtc_callback);

  // init_temperature();

  // enable interrupts
  sei();

  if (!protocol_start(FIRMWARE_VERSION, handleCanMessage))
  {
    signal_status();
    return -1;
  }

  initialize_pwm(0);

  // set defaults
  current_value.channel_a_level = device_config->channel_a_level_default;
  current_value.channel_b_level = device_config->channel_b_level_default;
  set_pwm_to_values(&current_value);

  // RSTCTRL.RSTFR &= ~RSTCTRL_WDRF_bm;  // set watchdog reset flag to 0 (this will be 1 after a watchdog reset)
  // WDT.CTRLA = WDT_PERIOD_8CLK_gc;     // 8ms check period, we will reset this every 1ms in the timer interrupt

  // Set sleep mode to STANDBY mode
  set_sleep_mode(SLEEP_MODE_STANDBY);
  sleep_enable();
  sleep_cpu();

  /* Replace with your application code */
  while (1)
  {
  }
}

