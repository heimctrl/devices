/*
 * temperature.h
 *
 * Created: 02/01/2022 16:51:26
 *  Author: Creator
 */ 


#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

extern int16_t temperature, decimals;

void init_temperature();
void read_temperature();

#endif /* TEMPERATURE_H_ */